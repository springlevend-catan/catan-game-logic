package be.kdg.catangamelogic.game_logic.action.delegator;

import be.kdg.catangamelogic.board_generator.business.BoardGenerator;
import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.action.exception.NotAResourceException;
import be.kdg.catangamelogic.game_logic.exception.InvalidBuildingTypeException;
import be.kdg.catangamelogic.game_logic.model.state.*;
import be.kdg.catangamelogic.model.tile.TileType;
import be.kdg.catangamelogic.game_logic.exception.NoDesertTileException;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.tile.Tile;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.model.action.*;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.exception.NotEnoughResourcesException;
import be.kdg.catangamelogic.shared.exception.RobberException;
import be.kdg.catangamelogic.shared.service.GameService;
import be.kdg.catangamelogic.shared.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
@Transactional
@TestPropertySource(locations = "classpath:application.properties")
public class ActionDelegatorTest {

    @Autowired
    private GameService gameService;
    @Autowired
    private UserService userService;
    private GameState gameState;
    private Player player1;
    private Player player2;
    private Game game;

    @Autowired
    private ActionDelegator delegator;

    @Before
    public void runBeforeTests() throws TileNotFoundException, NoDesertTileException, PlayerNotInGameException {
        User user1 = new User("ivan", "ivan@sladkov.com");
        User user2 = new User("bart", "bart@sladkov.com");
        userService.saveUser(user1);
        userService.saveUser(user2);
        player1 = new Player(user1, 0, true);
        player2 = new Player(user2, 1, false);

        List<Player> players = new ArrayList<>();
        players.add(player1);
        players.add(player2);
        game = new Game(players);
        game.setUrl("testUrl");
        game.setTiles(BoardGenerator.generateBoard());
        gameState = new GameState(game);
        game.setCurrentGameState(gameState);
        gameService.saveGame(game);
    }

    /**
     * Test if the method returns a game state.
     */
    @Test
    public void delegateAction() {
    }

    @Test
    public void endTurn() throws ActionDelegatingException {
        gameState.setTurnCounter(2);
        Turn turn1 = new Turn(player1, 0);
        Turn turn2 = new Turn(player2, 1);
        Action action1 = new Action(turn1, ActionType.END_TURN);
        Action action2 = new Action(turn2, ActionType.END_TURN);
        delegator.delegateAction(action1, gameState);
        Assert.assertEquals(gameState.getRequiredAction(), ActionType.DICE);
        Assert.assertEquals(gameState.getPlayerWithTurn().getUsername(), player2.getUser().getUsername());
        delegator.delegateAction(action2, gameState);
        System.out.println(gameState.getPlayerWithTurn());
        Assert.assertEquals(gameState.getPlayerWithTurn().getUsername(), player1.getUser().getUsername());
    }
    /*
    @Test
    public void checkResources() throws NoDesertTileException, PlayerNotInGameException {
        Action action = new Action();
        action.setType(ActionType.DICE);

        Tile tile = new Tile(50, 50, TileType.BRICK, 5);

        ArrayList<Tile> list = new ArrayList<>();
        list.add(new Tile(50, 51, TileType.DESERT, 0));
        list.add( (new Tile(51, 50, TileType.GRAIN, 9)));
        Building building1 = new Building(tile, 1, BuildingType.VILLAGE, list, new HashMap<>(), "ivan");
        game.setTiles(list);

        GameState resourceState = new GameState(game);

        List<Building> buildings = new ArrayList<>();

        buildings.add(building1);

        resourceState.setBuildings(buildings);

        Optional<Map<TileType, Integer>> stateOptional = delegator.checkResources(resourceState, 5);
        Assert.assertTrue(stateOptional.isPresent());
        Map<TileType, Integer> state = stateOptional.get();
        Assert.assertEquals(state.get(TileType.BRICK).intValue(), 1);
    }
    */

    @Test
    public void createDiceResourceTransactions() throws NoDesertTileException, PlayerNotInGameException, ActionDelegatingException {
        List<TileState> neighbours = new ArrayList<>();

        TileState tileState1 = new TileState(new Tile(50, 50, TileType.BRICK, 5));
        TileState tileState2 = new TileState(new Tile(51, 50, TileType.GRAIN, 9));
        TileState tileState3 = new TileState(new Tile(50, 51, TileType.DESERT, 5));

        neighbours.add(tileState2);
        neighbours.add(tileState3);

        Building building1 = new Building(tileState1, 3, BuildingType.VILLAGE, neighbours, null, "ivan", 0);
        Building building2 = new Building(tileState1, 3, BuildingType.VILLAGE, neighbours, null, "bart", 1);
        Building building3 = new Building(tileState1, 3, BuildingType.CITY, neighbours, null, "bart", 1);


        tileState1.addBuilding(building1);
        tileState1.addBuilding(building2);
        tileState1.addBuilding(building3);
        GameState gameState = new GameState(game);

        List<List<TileState>> tiles = gameState.getTileStates();

        tiles.add(building1.getAllTiles());
        gameState.setRequiredAction(ActionType.DICE);
        delegator.delegateAction(new DiceAction(game.getLastTurn(), ActionType.DICE, 2, 3), gameState);
        Assert.assertEquals(gameState.getPlayerStateByUsername("ivan").get().getResources().get(TileType.BRICK).intValue(), 1);
        Assert.assertEquals(gameState.getPlayerStateByUsername("bart").get().getResources().get(TileType.BRICK).intValue(), 3);
    }

    @Test
    public void resourceTransaction() throws NotEnoughResourcesException, PlayerNotInGameException, NotAResourceException {
        Map<TileType, Integer> map = new HashMap<>();
        map.put(TileType.BRICK, 5);
        ResourceTransaction action = new ResourceTransaction(game.getLastTurn(), ActionType.RESOURCE_TRANSACTION, null, player1.getUser().getUsername(), map, null);
        delegator.resourceTransactionAction(game.getCurrentGameState(), action);
        Assert.assertEquals(gameState.getPlayerStateByUsername(player1.getUser().getUsername())
                .orElseThrow(PlayerNotInGameException::new).getResources()
                .get(TileType.BRICK).intValue(), 5);
    }

    @Test(expected = NotEnoughResourcesException.class)
    public void tradeWithNoResources() throws NotEnoughResourcesException, NotAResourceException, PlayerNotInGameException {
        Map<TileType, Integer> map = new HashMap<>();
        map.put(TileType.BRICK, 5);
        ResourceTransaction action = new ResourceTransaction(game.getLastTurn(), ActionType.RESOURCE_TRANSACTION, player2.getUser().getUsername(), player1.getUser().getUsername(), map, map);
        delegator.resourceTransactionAction(game.getCurrentGameState(), action);
    }

    @Test
    public void moveRobber() throws ActionDelegatingException {
        Turn turn = new Turn(player1, 1);
        MoveRobber robber = new MoveRobber(turn, ActionType.MOVE_ROBBER, 2, 2);
        gameState.setRequiredAction(ActionType.MOVE_ROBBER);
        delegator.delegateAction(robber, gameState);
        Assert.assertArrayEquals(game.getCurrentGameState().getRobber(), new int[]{robber.getDestinationX(), robber.getDestinationY()});
    }

    @Test(expected = ActionDelegatingException.class)
    public void moveRobberToWater() throws ActionDelegatingException {
        Turn turn = new Turn(player1, 1);
        MoveRobber robber = new MoveRobber(turn, ActionType.MOVE_ROBBER, 0, 3);
        try {
            gameState.setRequiredAction(ActionType.MOVE_ROBBER);
            delegator.delegateAction(robber, gameState);
        } catch (ActionDelegatingException e) {
            Assert.assertEquals(e.getMessage(), "Can't place robber in water");
            throw e;
        }
    }

    @Test(expected = ActionDelegatingException.class)
    public void moveRobberToPreviousLocation() throws NoDesertTileException, ActionDelegatingException {
        Turn turn = new Turn(player1, 1);
        Tile tile = game.getTiles().stream().filter(t -> t.getTileType() == TileType.DESERT).findFirst().orElseThrow(NoDesertTileException::new);
        MoveRobber robber = new MoveRobber(turn, ActionType.MOVE_ROBBER, tile.getCoordinateX(), tile.getCoordinateY());
        try {
            gameState.setRequiredAction(ActionType.MOVE_ROBBER);
            delegator.delegateAction(robber, gameState);
        } catch (ActionDelegatingException e) {
            Assert.assertEquals(e.getMessage(), "You can't move the robber to it's previous location");
            throw e;
        }

    }

    @Test(expected = ActionDelegatingException.class)
    public void moveRobberToNonExistingTile() throws ActionDelegatingException {
        Turn turn = new Turn(player1, 1);
        MoveRobber robber = new MoveRobber(turn, ActionType.MOVE_ROBBER, 50, 3);
        try {
            gameState.setRequiredAction(ActionType.MOVE_ROBBER);
            delegator.delegateAction(robber, gameState);
        } catch (ActionDelegatingException e) {
            Assert.assertEquals(e.getMessage(), "Can't find selected tile");
            throw e;
        }
    }
    
    @Test
    public void buildVillageFirstTurn() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);
        gameState = buildgame.getCurrentGameState();

        delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 1, 1, 1), gameState);

        Building building1 = gameState.getBuildings().stream().filter(b -> b.getUsername().equals(playerBuild1.getUser().getUsername())).findFirst().get();

        Assert.assertEquals(building1.getTopLeftTile().getX(), 1);
        Assert.assertEquals(building1.getTopLeftTile().getY(), 1);
    }

    @Test(expected = ActionDelegatingException.class)
    public void buildVillageOnOtherBuilding() throws NoDesertTileException, TileNotFoundException, PlayerNotInGameException, ActionDelegatingException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);
        gameState = buildgame.getCurrentGameState();

        delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 1, 1, 1), gameState);

        try {
            delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 1, 1, 1), gameState);
        } catch (ActionDelegatingException e) {
            Assert.assertEquals(e.getMessage(), "Building already on spot x: 1 y: 1 i: 1");
            throw e;
        }
    }

    @Test(expected = ActionDelegatingException.class)
    public void buildVillageOnWater() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);
        gameState = buildgame.getCurrentGameState();

        try {
            delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 0, 1, 1), gameState);

        } catch (ActionDelegatingException e) {
            Assert.assertEquals(e.getMessage(), "A building can't be placed without a land tile");
            throw e;
        }
    }


    @Test()
    public void buildVillageNotOnTopLeft() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);
        gameState = buildgame.getCurrentGameState();
        delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 1, 2, 5), gameState);
        Building building1 = gameState.getBuildings().stream().filter(b -> b.getUsername().equals(playerBuild1.getUser().getUsername())).findFirst().get();
        Assert.assertEquals(building1.getTopLeftTile().getX(), 1);
        Assert.assertEquals(building1.getTopLeftTile().getY(), 1);
    }

    @Test()
    public void buildVillageOnTheBottomSide() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);
        gameState = buildgame.getCurrentGameState();
        delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 4, 2, 0), gameState);
        Building building1 = gameState.getBuildings().stream().filter(b -> b.getUsername().equals(playerBuild1.getUser().getUsername())).findFirst().get();
        Assert.assertEquals(building1.getTopLeftTile().getX(), 3);
        Assert.assertEquals(building1.getTopLeftTile().getY(), 2);
    }

    @Test()
    public void buildVillageWithResourcesRemoved() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);
        gameState = buildgame.getCurrentGameState();
        Player player = buildgame.getPlayerWithTurn();
        PlayerState playerState = gameState.getPlayerStateByUsername("ivan").get();
        Map<TileType, Integer> resources = new HashMap<>();
        resources.put(TileType.BRICK, 1);
        resources.put(TileType.WOOL, 1);
        resources.put(TileType.WOOD, 1);
        resources.put(TileType.GRAIN, 1);
        playerState.setResources(resources);
        // Go to turn where resourcesToGive need to be paid
        player.getTurnsPlayed().add(new Turn(player, 2));
        delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 4, 2, 0), gameState);
        Building building1 = gameState.getBuildings().stream().filter(b -> b.getUsername().equals(playerBuild1.getUser().getUsername())).findFirst().get();
        Assert.assertNotNull(building1);
        Assert.assertEquals((long) playerState.getResources().get(TileType.BRICK), 0);
        Assert.assertEquals((long) playerState.getResources().get(TileType.WOOD), 0);
        Assert.assertEquals((long) playerState.getResources().get(TileType.WOOL), 0);
        Assert.assertEquals((long) playerState.getResources().get(TileType.GRAIN), 0);

    }

    @Test()
    public void checkBuildingSpots() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException, InvalidBuildingTypeException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);

        gameState = buildgame.getCurrentGameState();
        Player player = buildgame.getPlayerWithTurn();
        PlayerState playerState = gameState.getPlayerStateByUsername("ivan").get();
        Map<TileType, Integer> resources = new HashMap<>();
        resources.put(TileType.BRICK, 1);
        resources.put(TileType.WOOL, 1);
        resources.put(TileType.WOOD, 1);
        resources.put(TileType.GRAIN, 1);
        playerState.setResources(resources);
        delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 1, 1, 0), gameState);

        List<List<TileState>> tilesWithSpots = gameState.getAvailableSpots("ivan", BuildingType.VILLAGE_BUILDING_SPOT);
        Assert.assertEquals("test", "test");
        System.out.println();
    }

    @Test()
    public void checkStreetBuildingSpots() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException, InvalidBuildingTypeException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);

        gameState = buildgame.getCurrentGameState();
        Player player = buildgame.getPlayerWithTurn();
        PlayerState playerState = gameState.getPlayerStateByUsername("ivan").get();
        Map<TileType, Integer> resources = new HashMap<>();
        resources.put(TileType.BRICK, 1);
        resources.put(TileType.WOOL, 1);
        resources.put(TileType.WOOD, 1);
        resources.put(TileType.GRAIN, 1);
        playerState.setResources(resources);
        delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 1, 1, 0), gameState);

        List<List<TileState>> tilesWithSpots = gameState.getAvailableSpots("ivan", BuildingType.STREET_BUILDING_SPOT);
        Assert.assertEquals("test", "test");
        System.out.println();
    }

    @Test(expected = ActionDelegatingException.class)
    public void buildVillageWithNotEnoughResources() throws ActionDelegatingException, NoDesertTileException, TileNotFoundException, PlayerNotInGameException {
        Player playerBuild1 = new Player(new User("ivan", "ivan@sladkov.com"), 0, true);
        List<Player> players = new ArrayList<>();
        players.add(playerBuild1);
        Game buildgame = new Game(players);
        gameState = buildgame.getCurrentGameState();
        Player player = buildgame.getPlayerWithTurn();
        PlayerState playerState = gameState.getPlayerStateByUsername("ivan").get();
        Map<TileType, Integer> resources = new HashMap<>();
        resources.put(TileType.BRICK, 1);
        resources.put(TileType.WOOL, 1);
        resources.put(TileType.WOOD, 1);
        resources.put(TileType.GRAIN, 0);

        playerState.setResources(resources);

        player.getTurnsPlayed().add(new Turn(player, 2));
        try {
            delegator.delegateAction(new BuildAction(buildgame.getLastTurn(), ActionType.BUILD_VILLAGE, 4, 2, 0), gameState);
        } catch (ActionDelegatingException e) {
            Assert.assertEquals(e.getMessage(), "Not enough resourcesToGive of type GRAIN expected 1 but got 0");
            Assert.assertTrue(gameState.getBuildings().stream().noneMatch(b -> b.getUsername().equals(playerBuild1.getUser().getUsername())));
            throw e;
        }

    }

    @Test()
    public void playYearOfPlenty() throws NoDesertTileException, TileNotFoundException, PlayerNotInGameException, ActionDelegatingException {
        Map<TileType, Integer> startResources = new HashMap<>();
        startResources.put(TileType.BRICK, 5);

        PlayerState playerState1 = gameState.getPlayerStateByUsername("ivan").get();
        PlayerState playerState2 = gameState.getPlayerStateByUsername("bart").get();


        playerState1.addCard(CardType.YEAR_OF_PLENTY);
        playerState1.setResources(startResources);
        playerState2.setResources(startResources);

        Action a = new PlayYearOfPlentyCard(game.getLastTurn(), ActionType.PLAY_YEAR_OF_PLENTY, TileType.BRICK);
        delegator.delegateAction(a, gameState);

        Assert.assertEquals(gameState.getPlayerStateByUsername("ivan").get().getDevelopmentCards().get(CardType.YEAR_OF_PLENTY).intValue(), 0);
    }

    @Test()
    public void playVictoryCard() throws ActionDelegatingException {
        PlayerState playerState1 = gameState.getPlayerStateByUsername("ivan").get();

        playerState1.addCard(CardType.VICTORY);

        Action a = new PlayVictoryCard(game.getLastTurn(), ActionType.PLAY_VICTORY_CARD);
        delegator.delegateAction(a, gameState);

        Assert.assertEquals(playerState1.getDevelopmentCards().get(CardType.VICTORY).intValue(), 0);
        Assert.assertEquals(playerState1.getPoints(), 1);

    }

    @Test()
    public void playKnightCard() throws ActionDelegatingException {
        PlayerState playerState1 = gameState.getPlayerStateByUsername("ivan").get();
        playerState1.addCard(CardType.KNIGHT);

        Action a = new PlayKnightCard(game.getLastTurn(), ActionType.PLAY_KNIGHT_CARD);
        delegator.delegateAction(a, gameState);

        Assert.assertEquals(playerState1.getDevelopmentCards().get(CardType.KNIGHT).intValue(), 0);
        Assert.assertEquals(playerState1.getNumberOfKnights(), 1);
        Assert.assertEquals(gameState.getRequiredAction(), ActionType.MOVE_ROBBER);
    }

    @Test()
    public void playInventionCard() throws ActionDelegatingException {
        PlayerState playerState1 = gameState.getPlayerStateByUsername("ivan").get();
        playerState1.addCard(CardType.INVENTION);
        Map<TileType, Integer> recourcesToGet = new HashMap<>();
        recourcesToGet.put(TileType.BRICK, 1);
        recourcesToGet.put(TileType.WOOL, 1);


        Action a = new PlayInventionCard(game.getLastTurn(), ActionType.PLAY_INVENTION_CARD, recourcesToGet);
        delegator.delegateAction(a, gameState);

        Assert.assertEquals(playerState1.getDevelopmentCards().get(CardType.INVENTION).intValue(), 0);
        Assert.assertEquals(playerState1.getResources().get(TileType.BRICK).intValue(), 1);
        Assert.assertEquals(playerState1.getResources().get(TileType.WOOL).intValue(), 1);
    }

    @Test()
    public void playStreetBuildingCard() throws ActionDelegatingException {
        PlayerState playerState1 = gameState.getPlayerStateByUsername("ivan").get();
        playerState1.addCard(CardType.STREET_BUILDING);

        Action a = new PlayStreetBuildingCard(game.getLastTurn(), ActionType.PLAY_STREET_BUILDING);
        delegator.delegateAction(a, gameState);

        Assert.assertEquals(playerState1.getDevelopmentCards().get(CardType.STREET_BUILDING).intValue(), 0);
        Assert.assertEquals(gameState.getRequiredAction(), ActionType.BUILD_TWO_STREETS);
    }


}
