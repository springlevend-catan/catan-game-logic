package be.kdg.catangamelogic.game_replay.controller;

import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.exception.NoDesertTileException;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.game_replay.manager.ReplayManager;
import be.kdg.catangamelogic.game_replay.service.ReplayService;
import be.kdg.catangamelogic.game_session.game.model.GameStateDTO;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.service.GameService;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api")
public class ReplayController {
    private GameService gameService;
    private final TokenConverter tokenConverter;
    private ReplayService replayService;
    private ReplayManager replayManager;

    public ReplayController(GameService gameService, TokenConverter tokenConverter, ReplayService replayService, ReplayManager replayManager) {
        this.gameService = gameService;
        this.tokenConverter = tokenConverter;
        this.replayService = replayService;
        this.replayManager = replayManager;
    }

    @GetMapping("replay/startReplay/{url}")
    public ResponseEntity<GameStateDTO> startReplay(@RequestHeader("Authorization") String token, @PathVariable String url) throws NoDesertTileException {
        System.out.println("replay is getting started for the game with the url : " + url);
        GameState gameState = replayManager.startReplay(url);
        return new ResponseEntity<>(new GameStateDTO(gameState), HttpStatus.OK);

    }

    @PostMapping("replay/next/{url}")
    public ResponseEntity<GameStateDTO> getNextAction(@RequestHeader("Authorization") String token,@PathVariable String url, @RequestBody GameState gameState) throws ActionDelegatingException {
        GameState state = (replayManager.doNextAction(url, gameState));
        return new ResponseEntity<>(new GameStateDTO(state), HttpStatus.OK);
    }


}
