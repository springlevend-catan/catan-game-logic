package be.kdg.catangamelogic.game_replay.manager;

import be.kdg.catangamelogic.game_logic.action.delegator.ActionDelegator;
import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.exception.NoDesertTileException;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.service.GameService;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ReplayManager {
    private GameService gameService;
    private ActionDelegator actionDelegator;
    private Map<String, List<Action>> actionsMap = new HashMap<>();
    private Map<String, Integer> actionCounterMap = new HashMap<>();

    public ReplayManager(GameService gameService, ActionDelegator actionDelegator) {
        this.gameService = gameService;
        this.actionDelegator = actionDelegator;
    }

    public GameState startReplay(String url) throws NoDesertTileException {
        Game game = gameService.getGameByUrl(url);
        List<Action> actions = new ArrayList<>();
        for (Player p:game.getPlayers()){
            for (Turn turn : p.getTurnsPlayed()) {
                actions.addAll(turn.getActions());
            }
        }
        actions = actions.stream().sorted(Comparator.comparing(Action::getActionTime)).collect(Collectors.toList());

        actionsMap.put(url, actions);
        actionCounterMap.put(url, 0);
        return new GameState(url, game.getTiles(), game.getPlayers());
    }

    public GameState doNextAction(String url, GameState gameState) throws ActionDelegatingException {
        if(actionCounterMap.get(url) < actionsMap.get(url).size()){
            Action action = actionsMap.get(url).get(actionCounterMap.get(url));
            gameState = actionDelegator.delegateAction(action, gameState);
            actionCounterMap.put(url, actionCounterMap.get(url) + 1);
        }
        return gameState;
    }
}
