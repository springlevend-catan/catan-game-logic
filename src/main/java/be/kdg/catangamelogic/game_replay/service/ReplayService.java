package be.kdg.catangamelogic.game_replay.service;

import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.service.GameService;
import org.springframework.stereotype.Component;

@Component
public class ReplayService {
    private GameService gameService;

    public ReplayService(GameService gameService) {
        this.gameService = gameService;
    }

}
