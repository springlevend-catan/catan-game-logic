package be.kdg.catangamelogic.model;

import be.kdg.catangamelogic.model.action.Action;

import java.util.ArrayList;
import java.util.List;

import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.service.GameService;
import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@JsonIdentityInfo(
        generator=ObjectIdGenerators.PropertyGenerator.class,
        property="turnId")
public class Turn implements Comparable<Turn> {
    @Id
    @GeneratedValue
    private long turnId;

    @ManyToOne(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "messageId")
    //@JsonIgnoreProperties(allowSetters=true)
    private Player player;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "turn")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
     //@JoinColumn(name = "actionId")
    private List<Action> actions;
    @Column
    private int actionCounter;


    @Column
    private int turnNumber;

    public Turn(Player player, int turnNumber) {
        this.player = player;
        this.turnNumber = turnNumber;
        this.actions = new ArrayList<>();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Turn turn = (Turn) o;
        return turnId == turn.turnId &&
                turnNumber == turn.turnNumber &&
                Objects.equals(actions, turn.actions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(turnId, actions, turnNumber);
    }

    @Override
    public int compareTo(Turn o) {
        return Comparator.comparing(Turn::getTurnNumber).compare(this, o);
    }

    @Override
    public String toString(){
        return "turn met turnId : "
                + turnId;
    }
}
