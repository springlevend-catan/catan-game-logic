package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
@NoArgsConstructor
public class RobberStealResource extends Action {
    @Column
    private String playerFrom;

    @Column
    private String playerTo;

    @Column
    private int randomResource;

    public RobberStealResource(Turn turn, ActionType type, String playerFrom, String playerTo, int randomResource) {
        super(turn, type);
        this.playerFrom = playerFrom;
        this.playerTo = playerTo;
        this.randomResource = randomResource;
    }
}
