package be.kdg.catangamelogic.model.action;


import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class DiceAction extends Action {

    @Column
    private int firstDie;

    @Column
    private int secondDie;

    public DiceAction(Turn turn, ActionType type, int firstDie, int secondDie) {
        super(turn, type);
        this.firstDie = firstDie;
        this.secondDie = secondDie;
    }
}
