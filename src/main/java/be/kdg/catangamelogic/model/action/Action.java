package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@JsonIdentityInfo(
        generator= ObjectIdGenerators.PropertyGenerator.class,
        property="actionId")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "descriminatorColumn")
public class Action implements Comparable<Action>{
    @Id
    @GeneratedValue
    private long actionId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "turnId")
    @JsonIgnore
    @Cascade(CascadeType.ALL)
    private Turn turn;

    @Column
    private LocalDateTime actionTime;
    @Column
    private int actionCounter;

    @Column
    private ActionType type;

    public Action(Turn turn, ActionType type) {
        this.turn = turn;
        this.actionCounter = turn.getActions().size();
        this.type = type;
        this.actionTime = LocalDateTime.now();
    }

    public String getUsername() {
        return this.turn.getPlayer().getUser().getUsername();
    }

    @Override
    public int compareTo(Action a) {
        return Integer.compare(this.actionCounter, a.actionCounter);
    }
}
