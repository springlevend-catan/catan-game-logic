package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.game_logic.model.state.BuildingType;
import be.kdg.catangamelogic.model.Turn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class BuildAction extends Action {

    // The coordinates of the building
    @Column
    private int x;

    @Column
    private int y;

    @Column
    private int index;


    public BuildAction(Turn turn, ActionType type, int x, int y, int index) {
        super(turn, type);
        this.x = x;
        this.y = y;
        this.index = index;
    }
}
