package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.tile.TileType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Data
@Entity
@NoArgsConstructor
public class PlayYearOfPlentyCard extends Action{
    @Transient
    private TileType resourcesToGet;

    public PlayYearOfPlentyCard(Turn turn, ActionType type, TileType resourcesToGet) {
        super(turn, type);
        this.resourcesToGet = resourcesToGet;
    }
}
