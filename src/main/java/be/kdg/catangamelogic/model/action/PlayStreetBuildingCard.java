package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class PlayStreetBuildingCard extends Action {
    public PlayStreetBuildingCard(Turn turn, ActionType type) {
        super(turn, type);
    }
}
