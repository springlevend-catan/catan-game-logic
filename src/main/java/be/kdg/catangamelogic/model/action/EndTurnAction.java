package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class EndTurnAction extends Action {

    public EndTurnAction(Turn turn, ActionType type) {
        super(turn, type);
    }
}
