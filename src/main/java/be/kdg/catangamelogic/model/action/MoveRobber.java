package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
public class MoveRobber extends Action{

    @Column
    private int destinationX;

    @Column
    private int destinationY;

    public MoveRobber(Turn turn, ActionType type, int destinationX, int destinationY) {
        super(turn, type);
        this.destinationX = destinationX;
        this.destinationY = destinationY;
    }
}
