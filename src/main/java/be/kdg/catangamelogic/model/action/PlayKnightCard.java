package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class PlayKnightCard extends Action{
    public PlayKnightCard(Turn turn, ActionType type) {
        super(turn, type);
    }
}
