package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class BuyDevelopmentCardAction extends Action{
    public BuyDevelopmentCardAction(Turn turn, ActionType type) {
        super(turn, type);
    }
}
