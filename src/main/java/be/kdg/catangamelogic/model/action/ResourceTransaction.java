package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.tile.TileType;
import be.kdg.catangamelogic.model.Turn;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
public class ResourceTransaction extends Action {
    @Column
    private String playerFrom;

    @Column
    private String playerTo;

    @Column
    @ElementCollection
    private Map<TileType, Integer> receivedResources;

    @Column
    @ElementCollection
    private Map<TileType, Integer> removedResources;

    public ResourceTransaction(Turn turn, ActionType type, String playerFrom, String playerTo, Map<TileType, Integer> receivedResources, Map<TileType, Integer> removedResources) {
        super(turn, type);
        this.playerFrom = playerFrom;
        this.playerTo = playerTo;
        this.receivedResources = receivedResources;
        this.removedResources = removedResources;
    }
}
