package be.kdg.catangamelogic.model.action;


import be.kdg.catangamelogic.model.Turn;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
public class PlayVictoryCard extends Action {

    public PlayVictoryCard(Turn turn, ActionType actionType) {
        super(turn, actionType);
    }
}
