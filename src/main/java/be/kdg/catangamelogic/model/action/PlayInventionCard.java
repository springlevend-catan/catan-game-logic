package be.kdg.catangamelogic.model.action;

import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.tile.TileType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.Map;

@Data
@Entity
@NoArgsConstructor
public class PlayInventionCard extends Action {

    @Transient
    @ElementCollection
    private Map<TileType, Integer> recourcesToGet;

    public PlayInventionCard(Turn turn, ActionType type, Map<TileType, Integer> recourcesToGet) {
        super(turn, type);
        this.recourcesToGet = recourcesToGet;
    }
}
