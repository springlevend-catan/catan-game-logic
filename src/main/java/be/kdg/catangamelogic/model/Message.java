package be.kdg.catangamelogic.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.aspectj.apache.bcel.util.Play;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
public class Message {
    @Id
    @GeneratedValue
    private long messageId;

    @ManyToOne
    @JoinColumn(name = "playerId")
    private Player player;

    @Column
    private String message;

    @Column
    private LocalDateTime time;

    public Message(Player player, String message, LocalDateTime time) {
        this.player = player;
        this.message = message;
        this.time = time;
    }
}
