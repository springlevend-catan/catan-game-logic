package be.kdg.catangamelogic.model.game;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
public class GameSettings {
    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    @JoinColumn(name = "messageId")
    private Game game;

    @Column
    private int size;

    public GameSettings(Game game, int size) {
        this.game = game;
        this.size = size;
    }
}
