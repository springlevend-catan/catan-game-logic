package be.kdg.catangamelogic.model.game;


import be.kdg.catangamelogic.board_generator.business.BoardGenerator;
import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.exception.NoDesertTileException;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.tile.Tile;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.*;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

@Data
@Entity
@NoArgsConstructor
public class Game {
    @Id
    @GeneratedValue
    private long gameId;

    @Column
    private boolean completed;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "messageId")
    @Cascade(CascadeType.ALL)
    @IndexColumn(name="Player_Index")
    private List<Player> players;

    @OneToOne
    private GameSettings gameSettings;

    @Column
    private String url;

    @Transient
    private GameState currentGameState;

    @Column
    private LocalDateTime gameStart;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "tileId")
    @Cascade(CascadeType.ALL)
    private List<Tile> tiles;


    public Game(List<Player> players) throws TileNotFoundException, NoDesertTileException, PlayerNotInGameException {
        this.players = players;
        this.completed = false;
        this.tiles = BoardGenerator.generateBoard();
        this.gameStart = LocalDateTime.now();
        this.url = String.valueOf(this.hashCode());
        //Add turn to the first player
        this.getPlayers().stream().min(Player::compareTo).ifPresent(player -> player.addTurn(new Turn(player,0)));
        this.currentGameState = new GameState(this);
    }

    public List<Turn> getSortedTurns() {
        List<Turn> turns = new LinkedList<>();
        for (Player player :
                this.getPlayers()) {
            turns.addAll(player.getTurnsPlayed());
        }
        turns.sort(Comparator.comparingInt(this::getTurnOrder).thenComparing(this::getTurnOrder));
        return turns;
    }

    private int getTurnOrder(Turn turn){
        if(players.size() == 1){
            return 0;
        }

        if(turn.getTurnNumber() == 1){
            return  players.size() - turn.getPlayer().getTurnOrder();
        }else{
            return turn.getPlayer().getTurnOrder();
        }
    }

    public Turn getLastTurn(){
        List<Turn> turns = this.getSortedTurns();
        return turns.get(turns.size()-1);
    }

    public Player getPlayerWithTurn() {
        int maxTurns =  players.stream().max(Comparator.comparingInt(p -> p.getTurnsPlayed().size())).get().getTurnsPlayed().size();
        if(maxTurns == 2){
            return players.stream().filter(p-> p.getTurnsPlayed().size()==maxTurns).min(Comparator.comparingInt(Player::getTurnOrder)).get();
        }else{
            return players.stream().filter(p-> p.getTurnsPlayed().size()==maxTurns).max(Comparator.comparingInt(Player::getTurnOrder)).get();
        }
    }

    public void addTurnForPlayer(String username) {
        Player player = players.stream().filter(f -> f.getUser().getUsername().equals(username)).findFirst().get();
        player.addTurn(new Turn(player, currentGameState.getTurnCounter()));
        currentGameState.setActionCounter(0);

    }
}
