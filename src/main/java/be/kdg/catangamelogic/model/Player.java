package be.kdg.catangamelogic.model;

import be.kdg.catangamelogic.model.game.Game;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator= ObjectIdGenerators.PropertyGenerator.class,
        property="playerId")
public class Player implements Comparable<Player> {
    @Id
    @GeneratedValue
    private long playerId;

    @ManyToOne
    @JoinColumn(name = "userId")
    @Cascade(CascadeType.MERGE)
    private User user;

    @ManyToOne
    @JoinColumn(name = "gameId")
    @Cascade(CascadeType.ALL)
    private Game game;

    @Column
    private int turnOrder;

    @OneToMany(fetch = FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    @Column
    private List<Turn> turnsPlayed;

    @Column
    private boolean isHost = false;

    @Column
    private boolean isComputer = false;

    public Player(User user, Game game, int turnOrder, boolean isHost) {
        this.user = user;
        this.game = game;
        this.turnOrder = turnOrder;
        this.isHost = isHost;
        this.turnsPlayed = new ArrayList<>();
    }

    public Player(User user, int turnOrder, boolean isHost) {
        this.user = user;
        this.turnOrder = turnOrder;
        this.isHost = isHost;
        this.turnsPlayed = new ArrayList<>();
    }

    public void addTurn(Turn turn) {
        this.turnsPlayed.add(turn);
    }

    @Override
    public int compareTo(Player o) {
        return Integer.compare(this.turnOrder, o.turnOrder);
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerId=" + playerId +
                ", user=" + user +
                ", game=" + game +
                ", turnOrder=" + turnOrder +
                ", isHost=" + isHost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return playerId == player.playerId &&
                turnOrder == player.turnOrder &&
                isHost == player.isHost &&
                Objects.equals(user, player.user) &&
                Objects.equals(game, player.game);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, user, game, turnOrder, isHost);
    }

    public void setComputer(boolean computer) {
        isComputer = computer;
    }

}
