package be.kdg.catangamelogic.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue
    private long userId;

    @Column(unique = true)
    private String username;

    @Column
    private String email;


    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }
}
