package be.kdg.catangamelogic.model.tile;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Comparator;

@Data
@Entity
@NoArgsConstructor
public class Tile implements Comparable<Tile>{
    @Id
    @GeneratedValue
    private long id;

    @Column
    private int coordinateX;

    @Column
    private int coordinateY;

    @Column
    private TileType tileType;

    @Column
    private int number;


    public Tile( int coordinateX, int coordinateY, TileType tileType, int number) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.tileType = tileType;
        this.number = number;
    }

    @Override
    public int compareTo(Tile tile) {
        return Tile.getComparator().compare(this, tile);
    }

    private static Comparator<Tile> getComparator(){
        return Comparator.comparing(Tile::getCoordinateX).thenComparing(Tile::getCoordinateY);
    }

}
