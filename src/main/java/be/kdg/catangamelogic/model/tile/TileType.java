package be.kdg.catangamelogic.model.tile;

import lombok.Data;
import lombok.Getter;

@Getter
public enum TileType {
    WOOD(0, false,true),
    GRAIN(1, false,true),
    WOOL(2, false,true),
    BRICK(3, false,true),
    ORE(4, false,true),
    DESERT(5, false,false),
    WATER(6, true,false),
    PORT(7, true,false),
    PORT_WOOD(8, true,false),
    PORT_GRAIN(9, true,false),
    PORT_WOOL(10, true,false),
    PORT_BRICK(11, true,false),
    PORT_ORE(12, true,false);

    private int index;
    private boolean isWater;
    private boolean isResource;

    TileType(int index, boolean isWater, boolean isResource) {
        this.index = index;
        this.isWater = isWater;
        this.isResource = isResource;
    }
}
