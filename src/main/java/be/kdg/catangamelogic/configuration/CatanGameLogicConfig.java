package be.kdg.catangamelogic.configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class CatanGameLogicConfig {
    @Bean
    public Random generator(){
        return new Random();
    }
}
