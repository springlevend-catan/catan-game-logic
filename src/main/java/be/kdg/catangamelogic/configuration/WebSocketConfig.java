package be.kdg.catangamelogic.configuration;

import be.kdg.catangamelogic.shared.websocket.ConnectionManager;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import be.kdg.catangamelogic.shared.util.QueryStringDecoder;
import be.kdg.catangamelogic.shared.websocket.WebSocket;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    private ConnectionManager connectionManager;
    private TokenConverter tokenConverter;

    @Autowired
    public WebSocketConfig(ConnectionManager connectionManager, TokenConverter tokenConverter) {
        this.connectionManager = connectionManager;
        this.tokenConverter = tokenConverter;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app")
                .enableSimpleBroker("/topic", "/notifications", "/chat");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws")
                .setAllowedOrigins("*")
                .withSockJS();

        registry.addEndpoint("/notification-socket")
                .setAllowedOrigins("*")
                .addInterceptors(new HandshakeInterceptor() {
                    @Override
                    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
                        Map<String, String> queryParams = QueryStringDecoder.getQueryParams(request.getURI().getQuery());

                        String sessionId = request.getURI().getPath().split("/")[3];
                        String token = queryParams.get("token");
                        String username = tokenConverter.convertToken(token).getSubject();

                        connectionManager.addSession(token, username, sessionId, WebSocket.NOTIFICATION);

                        return true;
                    }

                    @Override
                    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

                    }
                })
                .withSockJS();

        registry.addEndpoint("/chat-socket")
                .setAllowedOrigins("*")
                .addInterceptors(new HandshakeInterceptor() {
                    @Override
                    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) {
                        String queryString = request.getURI().getQuery();
                        Map<String, String> queryParams = QueryStringDecoder.getQueryParams(queryString);

                        String sessionId = request.getURI().getPath().split("/")[3];
                        String chatId = queryParams.get("id");
                        String username = tokenConverter.convertToken(queryParams.get("token")).getSubject();

                        connectionManager.addSession(chatId, username, sessionId, WebSocket.CHAT);

                        return true;
                    }

                    @Override
                    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

                    }
                })
                .withSockJS();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
