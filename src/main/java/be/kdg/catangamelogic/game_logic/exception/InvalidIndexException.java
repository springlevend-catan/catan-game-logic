package be.kdg.catangamelogic.game_logic.exception;

public class InvalidIndexException extends Throwable {
    public InvalidIndexException(String message) {
        super(message);
    }
}
