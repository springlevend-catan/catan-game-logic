package be.kdg.catangamelogic.game_logic.exception;

public class ActionNotFoundException extends Throwable {
    public ActionNotFoundException(String s) {
        super(s);
    }
}
