package be.kdg.catangamelogic.game_logic.trade.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TradeRequestDTO {
    private String username;
    private ArrayList resourcesToGive;
    private ArrayList resourcesToGet;
}
