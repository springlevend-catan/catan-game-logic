package be.kdg.catangamelogic.game_logic.trade.controller;

import be.kdg.catangamelogic.game_logic.trade.model.TradeRequestDTO;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import org.modelmapper.ModelMapper;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class TradeWebSocket {
    private final ModelMapper modelMapper;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final TokenConverter tokenConverter;


    public TradeWebSocket(ModelMapper modelMapper, SimpMessagingTemplate simpMessagingTemplate, TokenConverter tokenConverter) {
        this.modelMapper = modelMapper;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.tokenConverter = tokenConverter;
    }

    @MessageMapping("game/sendTradeRequest/{url}")
    public void sendTradeRequest(@DestinationVariable("url")String url, TradeRequestDTO tradeRequest){
        System.out.println("joined. sending out trade requests...");
        System.out.println(tradeRequest.getUsername());
        System.out.println("resources to get : " + tradeRequest.getResourcesToGet().size());
        System.out.printf("User %s traded a resource array with length : %d",tradeRequest.getUsername(),tradeRequest.getResourcesToGive().size());
        simpMessagingTemplate.convertAndSend("/topic/trade/" + url,tradeRequest);

    }
}
