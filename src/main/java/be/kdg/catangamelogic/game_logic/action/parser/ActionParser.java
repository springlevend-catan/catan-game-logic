package be.kdg.catangamelogic.game_logic.action.parser;

import be.kdg.catangamelogic.game_logic.exception.InvalidActionDataException;
import be.kdg.catangamelogic.game_logic.model.actiondata.*;
import be.kdg.catangamelogic.game_logic.exception.ActionNotFoundException;
import be.kdg.catangamelogic.game_session.game.model.ActionDTO;
import be.kdg.catangamelogic.model.action.ActionType;
import be.kdg.catangamelogic.model.action.BuildAction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ActionParser {

    private final ObjectMapper objectMapper;

    public ActionParser(ObjectMapper objectMapper) {

        this.objectMapper = objectMapper;
    }

    public ActionType getActionType(ActionDTO actionDTO) throws ActionNotFoundException {
        switch (actionDTO.getType()) {
            case "DICE":
                return ActionType.DICE;
            case "BUILD_VILLAGE":
                return ActionType.BUILD_VILLAGE;
            case "BUILD_STREET":
                return ActionType.BUILD_STREET;
            case "BUILD_CITY":
                return ActionType.BUILD_CITY;
            case "END_TURN":
                return ActionType.END_TURN;
            case "MOVE_ROBBER":
                return ActionType.MOVE_ROBBER;
            case "RESOURCE_TRANSACTION" :
                return ActionType.RESOURCE_TRANSACTION;
            case "PLAY_VICTORY_CARD":
                return ActionType.PLAY_VICTORY_CARD;
            case "PLAY_KNIGHT_CARD":
                return ActionType.PLAY_KNIGHT_CARD;
            case "PLAY_YEAR_OF_PLENTY_CARD":
                return ActionType.PLAY_YEAR_OF_PLENTY;
            case "PLAY_INVENTION_CARD":
                return ActionType.PLAY_INVENTION_CARD;
            case "BUY_DEVELOPMENT_CARD":
                return ActionType.BUILD_DEVELOPMENT;
            case "PLAY_STREET_BUILDING":
                return ActionType.PLAY_STREET_BUILDING;
            case "ROBBER_STEAL_RESOURCE":
                return ActionType.ROBBER_STEAL_RESOURCE;
            default:
                throw new ActionNotFoundException("Action with type " + actionDTO.getType() + " not found");
        }
    }

    public ActionData getActionData(ActionDTO actionDTO) throws ActionNotFoundException, InvalidActionDataException {
        switch (actionDTO.getType()) {
            case "DICE":
                return null;
            case "END_TURN":
                return null;
            case "BUILD_VILLAGE":
                return parseData(actionDTO.getValue(), BuildActionData.class);
            case "BUILD_STREET":
                return parseData(actionDTO.getValue(), BuildActionData.class);
            case "BUILD_CITY":
                return parseData(actionDTO.getValue(), BuildActionData.class);
            case "MOVE_ROBBER":
                return parseData(actionDTO.getValue(), MoveRobberData.class);
            case "RESOURCE_TRANSACTION":
                return parseData(actionDTO.getValue(), ResourceTransactionData.class);
            case "PLAY_VICTORY_CARD":
                return null;
            case "PLAY_KNIGHT_CARD":
                return null;
            case "PLAY_YEAR_OF_PLENTY_CARD":
                return parseData(actionDTO.getValue(), YearOfPlentyData.class);
            case "PLAY_INVENTION_CARD":
                return parseData(actionDTO.getValue(), InventionData.class);
            case "BUY_DEVELOPMENT_CARD":
                return null;
            case "PLAY_STREET_BUILDING":
                return null;
            case "ROBBER_STEAL_RESOURCE":
                return parseData(actionDTO.getValue(), RobberStealResourceData.class);
            default:
                throw new ActionNotFoundException("Action with type " + actionDTO.getType() + " not found");
        }
    }

    public ActionData parseData(String value, Class dataClass) throws InvalidActionDataException {
        try {
            System.out.println(value);
            return  (ActionData) objectMapper.readValue(value, dataClass);
        } catch (IOException e) {
            throw new InvalidActionDataException();
        }
    }
}
