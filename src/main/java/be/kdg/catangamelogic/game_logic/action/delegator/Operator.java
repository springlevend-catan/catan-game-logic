package be.kdg.catangamelogic.game_logic.action.delegator;

public interface Operator {
    boolean hasIndex(boolean firstCondition, boolean secondCondition);
}
