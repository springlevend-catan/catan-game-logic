package be.kdg.catangamelogic.game_logic.action.exception;

public class NotYourTurnException extends Throwable {
    public NotYourTurnException(String message) {
        super(message);
    }
}
