package be.kdg.catangamelogic.game_logic.action.service;


import be.kdg.catangamelogic.game_logic.action.repository.ActionRepository;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActionService {

    private final ActionRepository actionRepository;

    public ActionService(ActionRepository actionRepository) {
        this.actionRepository = actionRepository;
    }

    public Action saveAction(Action action){
        return actionRepository.save(action);
    }
    public List<Action> getActionsFromTurns(Turn turn) {
        return actionRepository.findAllByTurn(turn);

    }
}
