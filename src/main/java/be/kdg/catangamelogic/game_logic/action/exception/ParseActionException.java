package be.kdg.catangamelogic.game_logic.action.exception;

public class ParseActionException extends Throwable {
    public ParseActionException(String message) {
        super(message);
    }
}
