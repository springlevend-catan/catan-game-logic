package be.kdg.catangamelogic.game_logic.action.factory;

import be.kdg.catangamelogic.game_logic.action.parser.ActionParser;
import be.kdg.catangamelogic.game_logic.exception.ActionNotFoundException;
import be.kdg.catangamelogic.game_logic.model.actiondata.*;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.*;
import be.kdg.catangamelogic.model.tile.TileType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Component
public class ActionFactory {

    private final ActionParser actionParser;
    private final Random generator;


    public ActionFactory(ActionParser actionParser, Random generator) {
        this.actionParser = actionParser;
        this.generator = generator;
    }

    public Action createAction(ActionData actionData, ActionType actionType, Turn turn) throws ActionNotFoundException {
        switch (actionType) {
            case DICE:
                return createDiceAction(actionType, turn);
            case BUILD_STREET:
            case BUILD_VILLAGE:
            case BUILD_CITY:
                return createBuildAction(actionData, actionType, turn);
            case BUILD_DEVELOPMENT:
                return createBuildDevelopmentAction(actionType, turn);
            case RESOURCE_TRANSACTION:
                return createResourceTransaction(actionData, actionType, turn);
            case PLAY_VICTORY_CARD:
                return createPlayVictoryCardAction(actionType, turn);
            case PLAY_KNIGHT_CARD:
                return createPlayKnightCardAction(actionType, turn);
            case END_TURN:
                return createEndTurnAction(actionType, turn);
            case PLAY_YEAR_OF_PLENTY:
                return createPlayYearOfPlentyAction(actionType, actionData, turn);
            case MOVE_ROBBER:
                return createMoveRobberAction(actionData, actionType, turn);
            case PLAY_INVENTION_CARD:
                return createPlayInventionAction(actionType, actionData, turn);
            case PLAY_STREET_BUILDING:
                return createPlayStreetAction(actionType, turn);
            case ROBBER_STEAL_RESOURCE:
                return createRobberStealResourceAction(actionType, actionData, turn);
            default:
                throw new ActionNotFoundException("Action with type " + actionType.toString() + " not found");
        }
    }

    private Action createRobberStealResourceAction(ActionType actionType, ActionData actionData, Turn turn) {
        RobberStealResourceData data = (RobberStealResourceData) actionData;
        int randomResource = generator.nextInt(5);
        return new RobberStealResource(turn, actionType, data.getTakeFrom(), data.getGiveTo(), randomResource);
    }

    private Action createPlayStreetAction(ActionType actionType, Turn turn) {
        return new PlayStreetBuildingCard(turn, actionType);
    }

    private Action createPlayInventionAction(ActionType actionType, ActionData actionData, Turn turn) {
        InventionData data = (InventionData) actionData;

        // ORDER from array: Brick, Grain , Ore, Wood, Wool
        Map<TileType, Integer> recourcesToGet = new HashMap<>();
        recourcesToGet.put(TileType.BRICK, data.getResourcesToGet()[0]);
        recourcesToGet.put(TileType.GRAIN, data.getResourcesToGet()[1]);
        recourcesToGet.put(TileType.ORE, data.getResourcesToGet()[2]);
        recourcesToGet.put(TileType.WOOD, data.getResourcesToGet()[3]);
        recourcesToGet.put(TileType.WOOL, data.getResourcesToGet()[4]);
        return new PlayInventionCard(turn, actionType, recourcesToGet);
    }

    private Action createPlayYearOfPlentyAction(ActionType actionType, ActionData actionData, Turn turn) {
        YearOfPlentyData data = (YearOfPlentyData) actionData;
        TileType resource = TileType.WATER;
        System.out.println(data.getResourceToGet());
        switch (data.getResourceToGet()) {
            case "BRICK":
                resource = TileType.BRICK;
                break;
            case "ORE":
                resource = TileType.ORE;
                break;
            case "WOOD":
                resource = TileType.WOOD;
                break;
            case "WOOL":
                resource = TileType.WOOL;
                break;
            case "GRAIN":
                resource = TileType.GRAIN;
                break;
        }
        return new PlayYearOfPlentyCard(turn, actionType, resource);
    }

    private Action createPlayKnightCardAction(ActionType actionType, Turn turn) {
        return new PlayKnightCard(turn, actionType);
    }

    private Action createPlayVictoryCardAction(ActionType actionType, Turn turn) {
        return new PlayVictoryCard(turn, actionType);
    }

    private Action createBuildDevelopmentAction(ActionType actionType, Turn turn) {
        return new BuyDevelopmentCardAction(turn, actionType);
    }

    private Action createResourceTransaction(ActionData actionData, ActionType actionType, Turn turn) {
        ResourceTransactionData data = (ResourceTransactionData) actionData;
        Map<TileType, Integer> receivedResources = new HashMap<>();
        Map<TileType, Integer> removedResources = new HashMap<>();
        receivedResources.put(TileType.BRICK, data.getReceivedResources()[0]);
        receivedResources.put(TileType.GRAIN, data.getReceivedResources()[1]);
        receivedResources.put(TileType.ORE, data.getReceivedResources()[2]);
        receivedResources.put(TileType.WOOD, data.getReceivedResources()[3]);
        receivedResources.put(TileType.WOOL, data.getReceivedResources()[4]);
        removedResources.put(TileType.BRICK, data.getRemovedResources()[0]);
        removedResources.put(TileType.GRAIN, data.getRemovedResources()[0]);
        removedResources.put(TileType.ORE, data.getRemovedResources()[0]);
        removedResources.put(TileType.WOOD, data.getRemovedResources()[0]);
        removedResources.put(TileType.WOOL, data.getRemovedResources()[0]);
        return new ResourceTransaction(turn, actionType, data.getPlayerFrom(), data.getPlayerTo(), receivedResources, removedResources);
    }

    private Action createEndTurnAction(ActionType actionType, Turn turn) {
        return new EndTurnAction(turn, actionType);
    }

    private Action createBuildAction(ActionData actionData, ActionType actionType, Turn turn) {
        BuildActionData data = (BuildActionData) actionData;
        return new BuildAction(turn, actionType, data.getX(), data.getY(), data.getIndex());
    }

    private Action createDiceAction(ActionType actionType, Turn turn) {
        return new DiceAction(turn, actionType, generator.nextInt(6) + 1, generator.nextInt(6) + 1);
    }

    private Action createMoveRobberAction(ActionData actionData, ActionType actionType, Turn turn) {
        MoveRobberData data = (MoveRobberData) actionData;
        return new MoveRobber(turn, actionType, data.getX(), data.getY());
    }

}
