package be.kdg.catangamelogic.game_logic.action.exception;

public class NotAResourceException extends Throwable {
    public NotAResourceException(String message) {
        super(message);
    }
}
