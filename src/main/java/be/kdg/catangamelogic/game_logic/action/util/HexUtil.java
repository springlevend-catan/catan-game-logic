package be.kdg.catangamelogic.game_logic.action.util;

import com.google.common.collect.Lists;

import java.util.*;

public class HexUtil {
    public static int[] getNeighbouringIndeces(int index) {
        int[] indeces = new int[2];
        if (index == 5) {
            indeces[0] = 0;
        } else {
            indeces[0] = index + 1;
        }

        if (index == 0) {
            indeces[1] = 5;
        } else {
            indeces[1] = index - 1;
        }
        return indeces;
    }
    public static Optional<Integer> getComplementaryIndex(int index){
        if (index<=2){
            return Optional.of(index+3);
        }else if (index <= 5){
            return Optional.of(index-3);
        }
        return Optional.empty();
    }

    public static int[] getNeighbouringVillageIndeces(int index){
        List<Integer> indeces = new ArrayList<Integer>();

        if (index == 0 || index == 2 || index == 4){
            indeces.add(0);
            indeces.add(2);
            indeces.add(4);
            indeces.remove(Integer.valueOf(index));
        }else if(index == 1 || index == 3 || index == 5){
            indeces.add(1);
            indeces.add(3);
            indeces.add(5);
            indeces.remove(Integer.valueOf(index));
        }

        indeces.sort(Integer::compareTo);

        int[] i =  new int[2];

        for (int j = 0; j < indeces.size(); j++) {
            i[j] = indeces.get(j);
        }
        return i;
    }
}
