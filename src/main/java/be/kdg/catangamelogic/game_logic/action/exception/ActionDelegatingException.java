package be.kdg.catangamelogic.game_logic.action.exception;

public class ActionDelegatingException extends Throwable {
    public ActionDelegatingException(String message) {
        super(message);
    }
}
