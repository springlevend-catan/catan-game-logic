package be.kdg.catangamelogic.game_logic.action.delegator;

import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.action.factory.ActionFactory;
import be.kdg.catangamelogic.game_logic.action.manager.ActionManager;
import be.kdg.catangamelogic.game_logic.exception.ActionNotFoundException;
import be.kdg.catangamelogic.game_logic.exception.InvalidBuildingTypeException;
import be.kdg.catangamelogic.game_logic.model.actiondata.BuildActionData;
import be.kdg.catangamelogic.game_logic.model.actiondata.MoveRobberData;
import be.kdg.catangamelogic.game_logic.model.actiondata.RobberStealResourceData;
import be.kdg.catangamelogic.game_logic.model.state.Building;
import be.kdg.catangamelogic.game_logic.model.state.BuildingType;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.game_logic.model.state.TileState;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.game_session.game.manager.GameNotCalculableException;
import be.kdg.catangamelogic.game_session.game.manager.GameStateManager;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.action.ActionType;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.model.tile.TileType;
import be.kdg.catangamelogic.shared.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ComputerDelegator {
    private GameStateManager gameStateManager;
    private GameService gameService;
    private ActionManager actionManager;
    private ActionFactory actionFactory;
    private final Random random = new Random();

    private final Logger logger = LoggerFactory.getLogger(ComputerDelegator.class);

    private int lastX = 0;
    private int lastY = 0;
    private int lastIndex = 0;


    private ActionDelegator actionDelegator;

    public ComputerDelegator(GameStateManager gameStateManager, GameService gameService, ActionManager actionManager, ActionFactory actionFactory, ActionDelegator actionDelegator) {
        this.gameStateManager = gameStateManager;
        this.gameService = gameService;
        this.actionManager = actionManager;
        this.actionFactory = actionFactory;
        this.actionDelegator = actionDelegator;
    }

    public GameState delegateComputer(String url, String username) throws GameNotCalculableException, PlayerNotInGameException, ActionDelegatingException, ActionNotFoundException, InvalidBuildingTypeException, TileNotFoundException {
        GameState gameState = gameStateManager.getGameState(url, username);

        logger.info("User: " + username + " turn");

        //Check for required actions --> if none, go to the check and detect the computers possible actions
        if (gameState.getRequiredAction() != null) {
            doRequiredActions(gameState, username);
        } else {
            checkPossibleActions(gameState, username);
        }

        return gameState;
    }

    private void doRequiredActions(GameState gameState, String username) throws ActionDelegatingException, ActionNotFoundException, InvalidBuildingTypeException, TileNotFoundException {
        if (gameState.getRequiredAction() == ActionType.BUILD_VILLAGE) {
            logger.info(username + " has to build a village");
            buildVillage(gameState, username);
        }
        if (gameState.getRequiredAction() == ActionType.BUILD_STREET) {
            logger.info(username + " has to build a street");
            buildStreet(gameState, username);
        }
        if (gameState.getRequiredAction() == ActionType.DICE) {
            logger.info(username + " has to roll the dice");
            rollDice(gameState, username);
        }
        if (gameState.getRequiredAction() == ActionType.MOVE_ROBBER) {
            logger.info(username + " has to move the robber");
            moveRobber(gameState, username);
        }
        if (gameState.getRequiredAction() == ActionType.ROBBER_STEAL_RESOURCE){
            logger.info(username + " has to steal a resource");
            robberStealResource(gameState, username);
        }
    }

    private void robberStealResource(GameState gameState, String username) throws TileNotFoundException, ActionNotFoundException, ActionDelegatingException {
        Random random = new Random();
        Set<String> playerSet = gameState.getStealPlayers();
        playerSet.remove(username);
        String player = null;
        if (playerSet.size()!=0) {
            player = playerSet.toArray(new String[playerSet.size()])[random.nextInt(playerSet.size())];
        }
        Game game = gameService.getGameByUrl(gameState.getUrl());
        Turn turn = actionManager.createOrGetTurn(username, game, gameState);
        RobberStealResourceData data = new RobberStealResourceData(player, username);

        logger.info(username + " Stole from " + player);
        Action a = actionFactory.createAction(data, ActionType.ROBBER_STEAL_RESOURCE, turn);
        actionManager.doAction(a, gameState, username, game, turn);


    }

    private void moveRobber(GameState gameState, String username) throws ActionNotFoundException, ActionDelegatingException {
        List<TileState> allTiles = gameState.getTiles();
        allTiles = allTiles.stream().filter(x -> !x.getTileType().isWater()).collect(Collectors.toList());
        allTiles.remove(allTiles.stream().filter(x -> x.getX() == gameState.getRobber()[0] && x.getY() == gameState.getRobber()[1]).findFirst().get());
        Random r = new Random();
        TileState robberTile = allTiles.get(r.nextInt(allTiles.size()));
        Game game = gameService.getGameByUrl(gameState.getUrl());
        Turn turn = actionManager.createOrGetTurn(username, game, gameState);
        MoveRobberData moveRobberData = new MoveRobberData(robberTile.getX(), robberTile.getY());

        logger.info(username + " moves the robber to : (x: " + robberTile.getX() + ", y: " + robberTile.getY() + ")");

        Action a = actionFactory.createAction(moveRobberData, ActionType.MOVE_ROBBER, turn);
        actionManager.doAction(a, gameState, username, game, turn);

    }

    private void buildStreet(GameState gamestate, String username) throws ActionDelegatingException, ActionNotFoundException, InvalidBuildingTypeException, TileNotFoundException {
        Game game = gameService.getGameByUrl(gamestate.getUrl());
        Turn turn = actionManager.createOrGetTurn(username, game, gamestate);
        BuildActionData buildActionData;
        if(gamestate.getTurnCounter() <= 1){
             buildActionData = new BuildActionData(lastIndex, lastX, lastY);
            Action a = actionFactory.createAction(buildActionData, ActionType.BUILD_STREET, turn);
            logger.info(username + "builds street on : " + String.format("x: %s; y: %s; ix: %s", buildActionData.getX(), buildActionData.getY(), buildActionData.getIndex()));

            actionManager.doAction(a, gamestate, username, game, turn);
        }
        else{
            List<List<TileState>> tiles = gamestate.getAvailableSpots(username, BuildingType.STREET_BUILDING_SPOT);
            List<Building> buildingSpots = new ArrayList<>();
            for (List<TileState> row:tiles){
                for (TileState tile:row) {
                    for (Building b:tile.getBuildings()) {
                        if(b.getBuildingType() == BuildingType.STREET_BUILDING_SPOT){
                            buildingSpots.add(b);
                        }
                    }
                }
            }

            if(buildingSpots.size()> 0){
                Optional<Building> randomSpot = getRandomBuildingSpot(buildingSpots);
                if(randomSpot.isPresent()){
                 buildActionData  = new BuildActionData(randomSpot.get().getIndex(), randomSpot.get().getTopLeftTile().getX(), randomSpot.get().getTopLeftTile().getY());
                    Action a = actionFactory.createAction(buildActionData, ActionType.BUILD_STREET, turn);
                    logger.info(username + "builds street on : " + String.format("x: %s; y: %s; ix: %s", buildActionData.getX(), buildActionData.getY(), buildActionData.getIndex()));

                    actionManager.doAction(a, gamestate, username, game, turn);
                }
            }
        }
    }

    private void buildVillage(GameState gamestate, String username) throws InvalidBuildingTypeException, TileNotFoundException, ActionNotFoundException, ActionDelegatingException {
        List<List<TileState>> possibleTiles = gamestate.getAvailableSpots(gamestate.getPlayerWithTurn().getUsername(), BuildingType.VILLAGE_BUILDING_SPOT);
        Game game = gameService.getGameByUrl(gamestate.getUrl());
        Turn turn = actionManager.createOrGetTurn(username, game, gamestate);
        Random r = new Random();
        boolean buildingIsBuild= false;

        //TODO: refactor to efficient code
        Optional<TileState> tileToBuildOn = Optional.empty();
        Optional<Building> buildingSpot = Optional.empty();

        List<Building> allBuildingSpots = new ArrayList<>();
        //Find all tiles with available village building spots
        for (List<TileState> row : possibleTiles) {
            for (TileState tileState : row) {
                for (Building building : tileState.getBuildings()) {
                    if (building.getBuildingType() == BuildingType.VILLAGE_BUILDING_SPOT) {
                        allBuildingSpots.add(building);
                    }
                }
            }
        }

        //Place the first house on a random position
        if (gamestate.getTurnCounter() == 0) {
            buildingSpot = getRandomBuildingSpot(allBuildingSpots);
            tileToBuildOn = Optional.of(buildingSpot.get().getTopLeftTile());
        }
        //The second house, place at resources that the computer doesn't stand next to, order: (wood, brick, grain, wool, ore)
        if (gamestate.getTurnCounter() == 1) {
            //Find the first building of the computer
            Optional<Building> firstBuilding = Optional.empty();
            for (TileState tileState : gamestate.getTiles()) {
                for (Building b : tileState.getBuildings()) {
                    if (b.getUsername().equals(username)) {
                        firstBuilding = Optional.of(b);
                    }
                }
            }

            if (firstBuilding.isPresent()) {
                ArrayList<TileType> resourcesComputerHasNot = new ArrayList<>();
                resourcesComputerHasNot.add(TileType.ORE);
                resourcesComputerHasNot.add(TileType.BRICK);
                resourcesComputerHasNot.add(TileType.GRAIN);
                resourcesComputerHasNot.add(TileType.WOOD);
                resourcesComputerHasNot.add(TileType.WOOL);

                //Remove the resources the computer can receive
                for (TileState tile : firstBuilding.get().getAllTiles()) {
                    resourcesComputerHasNot.remove(tile.getTileType());
                }

                //Find tiles with the that are present in the list
                ArrayList<Building> buildingSpots = new ArrayList<>();
               /* for (List<TileState> row : possibleTiles) {
                    for (TileState tileState : row) {
                        //check if this tile has the resource
                        if (resourcesComputerHasNot.contains(tileState.getTileType())) {

                            //Check if the tile has available building spots
                            int buildingSpotsAvailable = 0;
                            for (Building b : tileState.getBuildings()) {
                                if (b.getBuildingType() == BuildingType.VILLAGE_BUILDING_SPOT) {
                                    buildingSpotsAvailable++;
                                }
                            }
                            if (buildingSpotsAvailable > 0) {
                                tilesWithMissingResources.add(tileState);
                            }
                        }
                    }
                }*/

                for (Building spot : allBuildingSpots) {
                        if(resourcesComputerHasNot.contains(spot.getTopLeftTile().getTileType())){
                            buildingSpots.add(spot);
                    }
                }

                //From the tiles with all the missing resources, select the tiles with the most 'important' resource
                ArrayList<Building> mostImportantSpots = new ArrayList<>();
                if (resourcesComputerHasNot.contains(TileType.BRICK)) {
                    mostImportantSpots.addAll(buildingSpots.stream().filter(x -> x.getTopLeftTile().getTileType() == TileType.BRICK).collect(Collectors.toList()));
                } else if (resourcesComputerHasNot.contains(TileType.WOOD)) {
                    mostImportantSpots.addAll(buildingSpots.stream().filter(x -> x.getTopLeftTile().getTileType()  == TileType.WOOD).collect(Collectors.toList()));
                } else if (resourcesComputerHasNot.contains(TileType.WOOL)) {
                    mostImportantSpots.addAll(buildingSpots.stream().filter(x -> x.getTopLeftTile().getTileType()  == TileType.WOOL).collect(Collectors.toList()));
                } else if (resourcesComputerHasNot.contains(TileType.GRAIN)) {
                    mostImportantSpots.addAll(buildingSpots.stream().filter(x -> x.getTopLeftTile().getTileType()  == TileType.GRAIN).collect(Collectors.toList()));
                } else if (resourcesComputerHasNot.contains(TileType.ORE)) {
                    mostImportantSpots.addAll(buildingSpots.stream().filter(x -> x.getTopLeftTile().getTileType()  == TileType.ORE).collect(Collectors.toList()));
                } else {
                    mostImportantSpots.addAll(buildingSpots);
                }


                //Get one of the tiles from that list to build on
                //If the list is empty take a random tile like before
                tileToBuildOn = Optional.of(mostImportantSpots.get(r.nextInt(mostImportantSpots.size())).getTopLeftTile());
                ArrayList<Building> villageBuildingSpots = tileToBuildOn.get().getBuildings().stream().filter(x -> x.getBuildingType() == BuildingType.VILLAGE_BUILDING_SPOT).collect(Collectors.toCollection(ArrayList::new));
                buildingSpot = Optional.of(villageBuildingSpots.get(r.nextInt(villageBuildingSpots.size())));
            } else {
                logger.error("First building not found");
            }
        }

       if(allBuildingSpots.size() > 0){
           buildingSpot = Optional.of(allBuildingSpots.get(random.nextInt(allBuildingSpots.size())));
           tileToBuildOn = Optional.of(buildingSpot.get().getTopLeftTile());
       }


        //Check the optionals, if both are not empty a building can be build
        if(buildingSpot.isPresent()){
            BuildActionData buildActionData = new BuildActionData(
                    buildingSpot.get().getIndex(),
                    tileToBuildOn.get().getX(),
                    tileToBuildOn.get().getY());
            logger.info(username + "builds village on : " + String.format("x: %s; y: %s; ix: %s", buildActionData.getX(), buildActionData.getY(), buildActionData.getIndex()));

            Action action = actionFactory.createAction(buildActionData, ActionType.BUILD_VILLAGE, turn);
            actionManager.doAction(action, gamestate, username, game, turn);

            lastX = tileToBuildOn.get().getX();
            lastY = tileToBuildOn.get().getY();
            lastIndex = buildingSpot.get().getIndex();
            buildingIsBuild = true;
        }else{
            logger.info("There are not possible building spots found for : " + username);
        }
    }

    private void rollDice(GameState gameState, String username) throws ActionNotFoundException, ActionDelegatingException {
        Game game = gameService.getGameByUrl(gameState.getUrl());
        Turn turn = actionManager.createOrGetTurn(username, game, gameState);
        Action action = actionFactory.createAction(null, ActionType.DICE, turn);

        actionManager.doAction(action, gameState, username, game, turn);
    }

    private void endTurn(GameState gameState, String username) throws ActionNotFoundException, ActionDelegatingException {
        Game game = gameService.getGameByUrl(gameState.getUrl());
        Turn turn = actionManager.createOrGetTurn(username, game, gameState);
        Action action = actionFactory.createAction(null, ActionType.END_TURN, turn);

        actionManager.doAction(action, gameState, username, game, turn);
    }

    private void checkPossibleActions(GameState gameState, String username) throws ActionNotFoundException, ActionDelegatingException, InvalidBuildingTypeException, TileNotFoundException {
        //Get the resources from the player
        Map<TileType, Integer> resources = gameState.getPlayerWithTurn().getResources();
        if (canBuild(BuildingType.CITY, resources)) {
            System.out.println("Build city");
        }
        if (canBuild(BuildingType.VILLAGE, resources)) {
            buildVillage(gameState, username);
        }
        if (canBuild(BuildingType.STREET, resources)) {
            buildStreet(gameState, username);
        }

        endTurn(gameState, username);
    }

    private boolean canBuild(BuildingType buildingType, Map<TileType, Integer> resources) {
        switch (buildingType) {
            case CITY:
                return resources.get(TileType.ORE) >= 3 && resources.get(TileType.GRAIN) >= 2;
            case VILLAGE:
                return resources.get(TileType.BRICK) >= 1 && resources.get(TileType.WOOD) >= 1 && resources.get(TileType.WOOL) >= 1 && resources.get(TileType.GRAIN) >= 1;
            case STREET:
                return resources.get(TileType.BRICK) >= 1 && resources.get(TileType.WOOD) >= 1;
        }
        return false;
    }

    private Optional<TileState> getRandomTile(List<TileState> tiles) {
        Random r = new Random();
        return Optional.of(tiles.get(r.nextInt(tiles.size())));
    }

    private Optional<Building> getRandomBuildingSpot(List<Building> buildingList){
        return Optional.of(buildingList.get(random.nextInt(buildingList.size())));
    }

}
