package be.kdg.catangamelogic.game_logic.action.delegator;

import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.action.exception.InvalidBuildingSpotException;
import be.kdg.catangamelogic.game_logic.action.exception.NotAResourceException;
import be.kdg.catangamelogic.game_logic.action.util.HexUtil;
import be.kdg.catangamelogic.game_logic.exception.InvalidIndexException;
import be.kdg.catangamelogic.game_logic.model.state.*;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.*;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.model.tile.Tile;
import be.kdg.catangamelogic.model.tile.TileType;
import be.kdg.catangamelogic.shared.exception.NotEnoughResourcesException;
import be.kdg.catangamelogic.shared.exception.RobberException;
import be.kdg.catangamelogic.shared.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ActionDelegator {
    private GameService gameService;
    private Player bank;
    private Game game;

    @Autowired
    public ActionDelegator(GameService gameService) {
        this.gameService = gameService;
    }

    public GameState delegateAction(Action action, GameState gameState) throws ActionDelegatingException {
        Player player = action.getTurn().getPlayer();
        game = gameService.getGameByUrl(gameState.getUrl());
        try {
            switch (action.getType()) {
                case BUILD_STREET:
                    return buildStreetAction(gameState, (BuildAction) action);
                case BUILD_CITY:
                    return buildCityAction(gameState, (BuildAction) action);
                case BUILD_DEVELOPMENT:
                    return buildDevelopmentAction(gameState, (BuyDevelopmentCardAction) action);
                case BUILD_VILLAGE:
                    return buildVillageAction(gameState, (BuildAction) action);
                case DICE:
                    return diceAction(gameState, (DiceAction) action);
                case END_TURN:
                    return endTurnAction(gameState, action);
                case PLAY_VICTORY_CARD:
                    return playVictoryCard(gameState, player, action);
                case PLAY_KNIGHT_CARD:
                    return playKnightCard(gameState, player, action);
                case PLAY_YEAR_OF_PLENTY:
                    return playYearOfPlenty(gameState, player, (PlayYearOfPlentyCard) action);
                case PLAY_INVENTION_CARD:
                    return playInventionCard(gameState, player, (PlayInventionCard) action);
                case MOVE_ROBBER:
                    return moveRobber(gameState, (MoveRobber) action);
                case RESOURCE_TRANSACTION:
                    return resourceTransactionAction(gameState, (ResourceTransaction) action);
                case PLAY_STREET_BUILDING:
                    return playStreetBuildingCard(gameState, player, action);
                case ROBBER_STEAL_RESOURCE:
                    return robberStealResource(gameState, (RobberStealResource)action);
            }

        } catch (PlayerNotInGameException | RobberException | NotEnoughResourcesException | NotAResourceException | TileNotFoundException | InvalidBuildingSpotException | InvalidIndexException e) {
          throw new ActionDelegatingException(e.getMessage());
        }

        return null;
    }

    /**
     * When a player rolls a 7, they can steal a random resource from another player if there is a building next to the robber.
     * This method transfers one resource between two players
     *
     * @param gameState The current gamestate
     * @param action The current action
     * @return returns an updated gamestate
     * @throws ActionDelegatingException
     * @throws PlayerNotInGameException
     */
    private GameState robberStealResource(GameState gameState, RobberStealResource action) throws ActionDelegatingException, PlayerNotInGameException {
        if (action.getPlayerFrom() == null){
            gameState.setRequiredAction(null);
            gameState.setActionCounter(gameState.getActionCounter() + 1);
            return gameState;
        }
        Optional<PlayerState> playerFromOptional = gameState.getPlayerStateByUsername(action.getPlayerFrom());
        if (playerFromOptional.isPresent()){
            PlayerState playerFrom = playerFromOptional.get();
            int randomResource = action.getRandomResource();
            Optional<TileType> optionalTileType = getRandomResourceFromPlayer(playerFrom, randomResource);

            if (optionalTileType.isPresent()){
                Map<TileType, Integer> stolenResource = new HashMap<>();
                stolenResource.put(optionalTileType.get(), 1);
                ResourceTransaction stealResources = new ResourceTransaction(action.getTurn(), ActionType.RESOURCE_TRANSACTION, playerFrom.getUsername(), action.getUsername(), stolenResource, null);
                gameState.setActionCounter(gameState.getActionCounter() + 1);
                gameState.setRequiredAction(null);
                return delegateAction(stealResources, gameState);
            }else {
                gameState.setActionCounter(gameState.getActionCounter() + 1);
                gameState.setRequiredAction(null);
                return gameState;
            }
        }else {
            throw new PlayerNotInGameException("Player not found in game");
        }

    }

    /**
     * The player should lose one random resource card. A random index is calculated in the actionfactory. This index can be used to get a random resource from an array of resourcetypes.
     * This method will check if the player owns a resource of the TileType with index "random". If the player does not own any of resources of this type, it will take the next available resource.
     *
     * @param playerFrom This player will lose one resource to the robber
     * @param random random index calculated in the actionfactory
     * @return returns a resourcetype with a value greater than zero
     */
    private Optional<TileType> getRandomResourceFromPlayer(PlayerState playerFrom, int random){
        Map<TileType, Integer> resources = playerFrom.getResources();
        TileType[] keys = resources.keySet().toArray(new TileType[resources.size()]);
        int resourceIndex = random;
        for (int i = 0; i < keys.length; i++) {
            System.out.println(resourceIndex);
            if (random>=keys.length){
                resourceIndex = 0;
            }
            if (resources.get(keys[resourceIndex]) > 0){
                return Optional.of(keys[resourceIndex]);
            }
            resourceIndex++;
        }
        return Optional.empty();
    }


    private GameState buildDevelopmentAction(GameState gameState, BuyDevelopmentCardAction action) throws PlayerNotInGameException, ActionDelegatingException {
        Optional<PlayerState> player = gameState.getPlayerStateByUsername(action.getUsername());

        if (player.isPresent()) {
            player.get();

            Map<TileType, Integer> price = new HashMap<>();
            price.put(TileType.GRAIN, 1);
            price.put(TileType.WOOL, 1);
            price.put(TileType.ORE, 1);
            Random random = new Random();
            int randomCardNumber = random.nextInt(5);

            if (randomCardNumber == 0) {
                player.get().addCard(CardType.VICTORY);
            }
            if (randomCardNumber == 1) {
                player.get().addCard(CardType.INVENTION);
            }
            if (randomCardNumber == 2) {
                player.get().addCard(CardType.STREET_BUILDING);
            }
            if (randomCardNumber == 3) {
                player.get().addCard(CardType.KNIGHT);
            }
            if (randomCardNumber == 4) {
                player.get().addCard(CardType.YEAR_OF_PLENTY);
            }

            ResourceTransaction removeResources = new ResourceTransaction(action.getTurn(), ActionType.RESOURCE_TRANSACTION, null, action.getUsername(), null, price);
            delegateAction(removeResources, gameState);

        } else {
            throw new PlayerNotInGameException("Player with username: " + action.getUsername() + " not in game");
        }
        gameState.setActionCounter(gameState.getActionCounter() + 1);
        return gameState;
    }

    private GameState buildCityAction(GameState gameState, BuildAction action) throws ActionDelegatingException, TileNotFoundException, InvalidBuildingSpotException {

        TileState tileState = gameState.findTile(action.getX(), action.getY()).get();

        Optional<Building> village = tileState.getBuildings().stream().filter(b -> b.getIndex() == action.getIndex()).findFirst();

        Map<TileType, Integer> price = new HashMap<>();

        if (!(action.getTurn().getTurnNumber() == 0 || action.getTurn().getTurnNumber() == 1)) {
            price.put(TileType.ORE, 3);
            price.put(TileType.GRAIN, 2);

        }

        if (village.isPresent()) {
            Building b = village.get();
            tileState.addBuilding(new Building(b.getTopLeftTile(), b.getIndex(), BuildingType.CITY, b.getNeighbours(), price, b.getUsername(), b.getPlayerOrder()));
            tileState.removeBuilding(b);
        } else {
            throw new InvalidBuildingSpotException("No village on spot x: " + action.getX() + " y: " + action.getY() + " i: " + action.getIndex());
        }

        ResourceTransaction removeResources = new ResourceTransaction(action.getTurn(), ActionType.RESOURCE_TRANSACTION, null, action.getUsername(), null, price);
        delegateAction(removeResources, gameState);
        gameState.getPlayerStateByUsername(action.getUsername()).ifPresent(playerState -> playerState.setPoints(playerState.getPoints() + 1));

        gameState.setActionCounter(gameState.getActionCounter() + 1);
        checkIfPlayerWon(gameState);
        return gameState;
    }

    /**
     * This method will add resourcesToGive to one player if this method get's triggered by a dice roll, or it will substract the resourcesToGive from 1 player
     * and add it to another if this method get's triggered by a trade.
     *
     * @param gameState the current game
     * @param action    the resource transaction action
     * @return the current game state
     * @throws NotEnoughResourcesException an exception if the player who trades resourcesToGive doesn't have enough resourcesToGive
     */
    public GameState resourceTransactionAction(GameState gameState, ResourceTransaction action) throws NotEnoughResourcesException, NotAResourceException, PlayerNotInGameException {
        PlayerState playerTo = gameState.getPlayerStateByUsername(action.getPlayerTo())
                .orElseThrow(PlayerNotInGameException::new);

        PlayerState playerFrom = gameState.getPlayerStateByUsername(action.getPlayerFrom())
                .orElse(null);

        if (action.getReceivedResources() != null) {
            if (playerFrom != null) {
                playerFrom.removeResources(action.getReceivedResources());
            }
            playerTo.addResources(action.getReceivedResources());

        }
        if (action.getRemovedResources() != null) {
            if (playerFrom != null) {
                playerFrom.addResources(action.getRemovedResources());
            }
            playerTo.removeResources(action.getRemovedResources());
        }
        return gameState;
    }

    /**
     * This action will move the robber to a new position chosen by a player. The robber can't be placed on it's previous location or on a water tile
     *
     * @param gameState the current gamestate
     * @param action    the move robber action
     * @return an updated gamestate with the new location of the robber
     * @throws RobberException Throws an exception when the next position of the robber is invalid
     */
    private GameState moveRobber(GameState gameState, MoveRobber action) throws RobberException, ActionDelegatingException {
        if (gameState.getRequiredAction()!=ActionType.MOVE_ROBBER){
            throw new ActionDelegatingException("Can't move robber when not required");
        }
        int[] previousRobber = gameState.getRobber();
        int[] nextRobber = {action.getDestinationX(), action.getDestinationY()};
        if (!Arrays.equals(previousRobber, nextRobber)) {
            Optional<TileState> oTile = gameState.getTiles().stream().filter(t -> t.getX() == nextRobber[0] && t.getY() == nextRobber[1]).findFirst();
            if (oTile.isPresent()) {
                TileState tile = oTile.get();
                if (!tile.getTileType().isWater()) {
                    gameState.setRobber(nextRobber);
                    gameState.setRequiredAction(ActionType.ROBBER_STEAL_RESOURCE);
                    System.out.println("move robber gaat zijn action counter setten");
                    gameState.setActionCounter(gameState.getActionCounter() + 1);
                    return gameState;
                } else {
                    throw new RobberException("Can't place robber in water");
                }
            } else {
                throw new RobberException("Can't find selected tile");
            }

        } else {
            throw new RobberException("You can't move the robber to it's previous location");
        }
    }


    private GameState playYearOfPlenty(GameState gameState, Player player, PlayYearOfPlentyCard action) throws PlayerNotInGameException, ActionDelegatingException {
        Optional<PlayerState> playerSendResourcesTo = gameState.getPlayerStateByUsername(action.getUsername());

        if (playerSendResourcesTo.isPresent() && playerSendResourcesTo.get().getDevelopmentCards().get(CardType.YEAR_OF_PLENTY) > 0) {
            playerSendResourcesTo.get().removeCard(CardType.YEAR_OF_PLENTY);
            for (PlayerState playerTakeResourcesFrom : gameState.getPlayerStates()) {
                if (playerTakeResourcesFrom != playerSendResourcesTo.get()) {
                    if (playerTakeResourcesFrom.getResources().get(action.getResourcesToGet()) > 0) {
                        int amountOfResources = playerTakeResourcesFrom.getResources().get(action.getResourcesToGet());

                        Map<TileType, Integer> resources = new HashMap<>();
                        resources.put(action.getResourcesToGet(), amountOfResources);

                        ResourceTransaction removeResources = new ResourceTransaction(action.getTurn(),
                                ActionType.RESOURCE_TRANSACTION,
                                playerTakeResourcesFrom.getUsername(),
                                action.getUsername(),
                                resources,
                                null);

                        gameState = delegateAction(removeResources, gameState);
                    }
                }
            }


        } else {
            throw new PlayerNotInGameException("Player: " + action.getUsername() + " is not in game");
        }
        gameState.setActionCounter(gameState.getActionCounter() + 1);
        return gameState;

    }

    private GameState playKnightCard(GameState gameState, Player player, Action action) throws PlayerNotInGameException {
        Optional<PlayerState> playerState = gameState.getPlayerStateByUsername(action.getUsername());
        if (playerState.isPresent() && playerState.get().getDevelopmentCards().get(CardType.KNIGHT) > 0) {
            playerState.get().removeCard(CardType.KNIGHT);
            playerState.get().setNumberOfKnights(playerState.get().getNumberOfKnights() + 1);
            gameState.setRequiredAction(ActionType.MOVE_ROBBER);
        } else {
            throw new PlayerNotInGameException("Player: " + action.getUsername() + " not in game");
        }
        gameState.setActionCounter(gameState.getActionCounter() + 1);
        checkIfPlayerWon(gameState);
        return gameState;
    }

    private GameState playVictoryCard(GameState gameState, Player player, Action action) throws PlayerNotInGameException {
        Optional<PlayerState> playerState = gameState.getPlayerStateByUsername(action.getUsername());
        if (playerState.isPresent() && playerState.get().getDevelopmentCards().get(CardType.VICTORY) > 0) {
            playerState.get().removeCard(CardType.VICTORY);
            playerState.get().setPoints(playerState.get().getPoints() + 1);
            gameState.setActionCounter(gameState.getActionCounter() + 1);
            checkIfPlayerWon(gameState);
            return gameState;
        } else {
            throw new PlayerNotInGameException("Player: " + action.getUsername() + " not in game");
        }
    }

    private GameState playInventionCard(GameState gameState, Player player, PlayInventionCard action) throws PlayerNotInGameException, ActionDelegatingException {
        Optional<PlayerState> playerState = gameState.getPlayerStateByUsername(action.getUsername());
        if(playerState.isPresent()){
            playerState.get().removeCard(CardType.INVENTION);
            ResourceTransaction getResources = new ResourceTransaction(action.getTurn(),
                    ActionType.RESOURCE_TRANSACTION,
                    null,
                    action.getUsername(),
                    action.getRecourcesToGet(),
                    null);


            gameState = delegateAction(getResources, gameState);

        }else{
            throw new PlayerNotInGameException("Player: " + action.getUsername() + "not in game");
        }
        gameState.setActionCounter(gameState.getActionCounter() + 1);
        return gameState;
    }

    private GameState playStreetBuildingCard(GameState gameState, Player player, Action action) throws PlayerNotInGameException {
        Optional<PlayerState> playerState = gameState.getPlayerStateByUsername(action.getUsername());
        if(playerState.isPresent()) {
            playerState.get().removeCard(CardType.STREET_BUILDING);
        }else{
            throw new PlayerNotInGameException("Player: " + action.getUsername() + "not in game");
        }
        gameState.setRequiredAction(ActionType.BUILD_TWO_STREETS);
        gameState.setActionCounter(gameState.getActionCounter() + 1);
        checkIfPlayerWon(gameState);
        return gameState;
    }

    private GameState endTurnAction(GameState gameState, Action action) throws PlayerNotInGameException {
        gameState.endTurn();


        if (gameState.getTurnCounter() <= 1) {
            gameState.setRequiredAction(ActionType.BUILD_VILLAGE);
        } else {
            gameState.setRequiredAction(ActionType.DICE);
        }
        gameState.setActionCounter(gameState.getActionCounter() + 1);
        return gameState;
    }

    /**
     * This method will emulate a dice roll, then checks if a player will receive resourcesToGive, and possibly trigger a robber action.
     * If a player will receive a resource, then we will handle a new resource Ttransaction.
     *
     * @param gameState the current game
     * @param action    the dice action
     * @return the current GameState
     */
    private GameState diceAction(GameState gameState, DiceAction action) throws ActionDelegatingException {
        if (gameState.getRequiredAction()!=ActionType.DICE){
            throw new ActionDelegatingException("Can't roll dice in the middle of a turn");
        }
        int diceRoll1 = action.getFirstDie();
        int diceRoll2 = action.getSecondDie();

        Turn turn = action.getTurn();
        DiceAction diceAction = new DiceAction(turn, action.getType(), diceRoll1, diceRoll2);
        gameState.setDice(new int[]{diceRoll1, diceRoll2});

        if (diceRoll1 + diceRoll2 == 7) {
            gameState.setRequiredAction(ActionType.MOVE_ROBBER);
            gameState.setActionCounter(gameState.getActionCounter() + 1);
            return gameState;
        } else {
            int thrownNumber = diceRoll1 + diceRoll2;

            for (ResourceTransaction rt : createDiceResourceTransactions(gameState, thrownNumber, turn)) {
                gameState = delegateAction(rt, gameState);
            }
            gameState.setRequiredAction(null);
            gameState.setActionCounter(gameState.getActionCounter() + 1);
            return gameState;
        }
    }

    public List<ResourceTransaction> createDiceResourceTransactions(GameState gameState, int number, Turn turn) {
        List<ResourceTransaction> rtList = new ArrayList<>();

        //Get all the Tiles with at least one building
        List<TileState> tilesWithBuildingAndNumber = gameState.getTiles().stream().filter(t ->
                !t.getBuildings().isEmpty()
                        && t.getBuildings().stream().anyMatch(Building::isNotAStreet)
                        && t.getBuildings().stream().anyMatch(b -> b.neighboursTileWithNumber(number))
        ).collect(Collectors.toList());

        HashMap<String, HashMap<TileType, Integer>> resourcePerPlayer = new HashMap<>();

        for (TileState ts : tilesWithBuildingAndNumber) {
            if(!(gameState.getRobber()[0]==ts.getX() && gameState.getRobber()[0]== ts.getY())){
                for (Building b : ts.getBuildings()) {
                    resourcePerPlayer.putIfAbsent(b.getUsername(), new HashMap<>());
                    b.getResources(number).ifPresent(r -> r.forEach((k, v) -> resourcePerPlayer.get(b.getUsername()).merge(k, v, Integer::sum)));
                }
            }

        }

        for (Map.Entry<String, HashMap<TileType, Integer>> entry : resourcePerPlayer.entrySet()) {
            rtList.add(new ResourceTransaction(turn, ActionType.RESOURCE_TRANSACTION, null, entry.getKey(), entry.getValue(), null));
        }
        return rtList;
    }

    private GameState buildStreetAction(GameState gameState, BuildAction action) throws TileNotFoundException, InvalidBuildingSpotException, ActionDelegatingException, PlayerNotInGameException, InvalidIndexException {
        List<TileState> tiles = new ArrayList<>();
        gameState.findTile(action.getX(), action.getY()).ifPresent(tiles::add);

        gameState.getNeighbour(action.getX(), action.getY(), action.getIndex()).ifPresent(tiles::add);
        TileState topLeftResourceTile = getTopLeftResourceTile(tiles);

        tiles.remove(topLeftResourceTile);
        int index = action.getIndex();
        if (topLeftResourceTile.getY() != action.getY() || topLeftResourceTile.getX() != action.getX()) {
            index = HexUtil.getComplementaryIndex(index).get();
        }

        Map<TileType, Integer> price = new HashMap<>();

        if (!(action.getTurn().getTurnNumber() == 0 || action.getTurn().getTurnNumber() == 1 || gameState.getRequiredAction() == ActionType.BUILD_TWO_STREETS || gameState.getRequiredAction() == ActionType.BUILD_SECOND_STREET)) {
            price.put(TileType.WOOD, 1);
            price.put(TileType.BRICK, 1);

            ResourceTransaction removeResources = new ResourceTransaction(action.getTurn(), ActionType.RESOURCE_TRANSACTION, null, action.getUsername(), null, price);
            delegateAction(removeResources, gameState);
        }

        Building building = new Building(topLeftResourceTile, index, BuildingType.STREET, tiles, price, action.getUsername(), action.getTurn().getPlayer().getTurnOrder());

        int x = topLeftResourceTile.getX();
        int y = topLeftResourceTile.getY();

        if (gameState.isAvailableStreetSpot(x, y, index, action.getUsername())) {
            topLeftResourceTile.addBuilding(building);
        } else {
            throw new InvalidBuildingSpotException(String.format("Street already on spot x: %d y: %d i: %d", x, y, index));
        }

        // TODO: calculate the longest route of the player;
        gameState.calculateLongestRoute(action.getUsername());

        if (gameState.getTurnCounter() <= 1) {
            this.endTurnAction(gameState, new EndTurnAction(action.getTurn(), action.getType()));
            // Pass the turn to the next player, depends 1 => 2 => 3 => 4 => 4 => 3 => 2 => 1 => First Actual turn: 1 => normal progression
        }

        if(gameState.getRequiredAction() == ActionType.BUILD_SECOND_STREET){
            gameState.setRequiredAction(null);
        }
        if(gameState.getRequiredAction() == ActionType.BUILD_TWO_STREETS){
            gameState.setRequiredAction(ActionType.BUILD_SECOND_STREET);
        }
        //gameState.setActionCounter(gameState.getActionCounter() + 1);
        checkIfPlayerWon(gameState);
        return gameState;
    }

    private void setActionCounter() {
        game.getSortedTurns().get(game.getSortedTurns().size() -1).setActionCounter(game.getSortedTurns().get(game.getSortedTurns().size() -1).getActionCounter() + 1);
    }


    /**
     * @param gameState the current Gamestate that will be returned modified
     * @param action    the action which contains the necessary information to correctly handle the action
     * @return the modified gameState with an added Village and the correct resourcesToGive removed from the correct player
     * @throws TileNotFoundException        thrown when the neighbouring tiles of the village can't be found
     * @throws ActionDelegatingException    thrown when the delegating of the resourceTransaction can't be completed correctly
     * @throws InvalidBuildingSpotException thrown when the building can't be placed on the correct buildingSpot
     */
    private GameState buildVillageAction(GameState gameState, BuildAction action) throws TileNotFoundException, ActionDelegatingException, InvalidBuildingSpotException {
        List<TileState> tiles = gameState.getNeighbours(action.getX(), action.getY(), action.getIndex());
        // Optional<TileState> ownTile = gameState.findTile(action.getX(), action.getY());
        // System.out.println(ownTile);
        int index = action.getIndex();
        String username = action.getUsername();

        TileState topLeftResourceTile = getTopLeftResourceTile(tiles);

        HashMap<Integer, TileState> n = gameState.getNeighboursWithIndex(action.getX(), action.getY(), index);

        for (Integer i : n.keySet()) {
            if (n.get(i).getX() == topLeftResourceTile.getX() && n.get(i).getY() == topLeftResourceTile.getY()) {
                index = i;
            }
        }

        tiles.remove(topLeftResourceTile);

        Map<TileType, Integer> price = new HashMap<>();

        int x = topLeftResourceTile.getX();
        int y = topLeftResourceTile.getY();

        if (!(action.getTurn().getTurnNumber() == 0 || action.getTurn().getTurnNumber() == 1)) {
            price.put(TileType.BRICK, 1);
            price.put(TileType.GRAIN, 1);
            price.put(TileType.WOOD, 1);
            price.put(TileType.WOOL, 1);

            ResourceTransaction removeResources = new ResourceTransaction(action.getTurn(), ActionType.RESOURCE_TRANSACTION, null, action.getUsername(), null, price);
            delegateAction(removeResources, gameState);
        } else {
            gameState.setRequiredAction(ActionType.BUILD_STREET);
        }

        Building building = new Building(topLeftResourceTile, index, BuildingType.VILLAGE, tiles, price, username, action.getTurn().getPlayer().getTurnOrder());

        //Check if the spot is available
        if (gameState.isAvailableVillageSpot(x, y, index, username) && gameState.checkDistance(x, y, index, username)) {
            // ownTile.get().addBuilding(building);
            topLeftResourceTile.addBuilding(building);
        } else {
            throw new InvalidBuildingSpotException(String.format("Building already on spot x: %d y: %d i: %d", x, y, index));
        }
        Optional<PlayerState> stateplayer = gameState.getPlayerStateByUsername(action.getUsername());
        gameState.getPlayerStateByUsername(action.getUsername()).ifPresent(playerState -> playerState.setPoints(playerState.getPoints() + 1));

        receiveSecondVillageResources(gameState, building, action);
        gameState.setLastAction(action);
        gameState.setActionCounter(gameState.getActionCounter() + 1);
        checkIfPlayerWon(gameState);
        return gameState;
    }

    private void receiveSecondVillageResources(GameState gameState, Building building, BuildAction action) throws ActionDelegatingException {
        if (gameState.getTurnCounter() == 1) {
            HashMap<TileType, Integer> resources = new HashMap<>();
            for (TileState t : building.getAllTiles()) {

                if (t.getTileType().isResource()) {
                    resources.putIfAbsent(t.getTileType(), 0);
                    resources.merge(t.getTileType(), 1, Integer::sum);
                }
            }
            ResourceTransaction removeResources = new ResourceTransaction(action.getTurn(), ActionType.RESOURCE_TRANSACTION, null, action.getUsername(), resources, null);
            delegateAction(removeResources, gameState);
        }
    }

    public TileState getTopLeftResourceTile(List<TileState> tiles) throws InvalidBuildingSpotException {
        TileState topLeft = null;

        for (TileState tile : tiles) {
            if (!tile.getTileType().isWater()) {
                if (topLeft == null) {
                    topLeft = tile;
                }
                if (tile.getX() < topLeft.getX()) {
                    topLeft = tile;
                } else if (tile.getX() == topLeft.getX() && tile.getY() < topLeft.getY()) {
                    topLeft = tile;
                }
            }
        }
        if (topLeft == null) {
            throw new InvalidBuildingSpotException("A building can't be placed without a land tile");
        }
        return topLeft;
    }

    private void checkIfPlayerWon(GameState gameState) {
        if(gameState.getPlayerWithTurn().getPoints() >= 10){
            gameState.setCompleted(true);
            Game game = gameService.getGameByUrl(gameState.getUrl());
            game.setCompleted(true);
        }
    }
}
