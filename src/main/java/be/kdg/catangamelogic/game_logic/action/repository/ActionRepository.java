package be.kdg.catangamelogic.game_logic.action.repository;

import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActionRepository extends JpaRepository<Action, Long> {
    List<Action> findAllByTurn(Turn turn);
}
