package be.kdg.catangamelogic.game_logic.action.manager;

import be.kdg.catangamelogic.game_logic.action.delegator.ActionDelegator;
import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.action.exception.NotYourTurnException;
import be.kdg.catangamelogic.game_logic.action.exception.ParseActionException;
import be.kdg.catangamelogic.game_logic.action.factory.ActionFactory;
import be.kdg.catangamelogic.game_logic.action.parser.ActionParser;
import be.kdg.catangamelogic.game_logic.action.service.ActionService;
import be.kdg.catangamelogic.game_logic.exception.ActionNotFoundException;
import be.kdg.catangamelogic.game_logic.exception.InvalidActionDataException;
import be.kdg.catangamelogic.game_logic.model.actiondata.ActionData;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.game_logic.model.state.PlayerState;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.game_session.game.manager.GameNotCalculableException;
import be.kdg.catangamelogic.game_session.game.manager.GameStateManager;
import be.kdg.catangamelogic.game_session.game.model.ActionDTO;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.action.ActionType;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.service.GameService;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ActionManager {

    private final GameStateManager gameStateManager;

    private final TokenConverter tokenConverter;

    private final ActionFactory actionFactory;

    private final ActionDelegator actionDelegator;

    private final GameService gameService;

    private final ActionParser actionParser;

    private final ActionService actionService;

    private final Logger logger = LoggerFactory.getLogger(ActionManager.class);

    public ActionManager(GameStateManager gameStateManager, TokenConverter tokenConverter, ActionFactory actionFactory, ActionDelegator actionDelegator, GameService gameService, ActionParser actionParser, ActionService actionService) {
        this.gameStateManager = gameStateManager;
        this.tokenConverter = tokenConverter;
        this.actionFactory = actionFactory;
        this.actionDelegator = actionDelegator;
        this.gameService = gameService;
        this.actionParser = actionParser;
        this.actionService = actionService;
    }

    public GameState processAction(String url, ActionDTO actionDTO) throws PlayerNotInGameException, GameNotCalculableException, ActionDelegatingException, ParseActionException, NotYourTurnException {
        String username = tokenConverter.convertToken(actionDTO.getToken()).getSubject();


        Game game = gameService.getGameByUrl(url);
        GameState gameState = gameStateManager.getGameState(url, username);

        if (!username.equals(gameState.getPlayerWithTurn().getUsername()) &&  !actionDTO.getType().equals(ActionType.RESOURCE_TRANSACTION.toString())){
            throw new NotYourTurnException("Not your turn");
        }

        game.setCurrentGameState(gameState);

        Turn turn = createOrGetTurn(username, game, gameState);

        Action action = parseAction(actionDTO, turn);

        if (gameState.getRequiredAction()!=null && gameState.getRequiredAction()!=action.getType()){
            if(gameState.getRequiredAction()==ActionType.BUILD_SECOND_STREET||gameState.getRequiredAction()==ActionType.BUILD_TWO_STREETS){
                if (action.getType()!=ActionType.BUILD_STREET){
                    throw new ActionDelegatingException("Not the required action");

                }
            }else{
                throw new ActionDelegatingException("Not the required action");
            }
        }
        gameState = doAction(action, gameState, username, game, turn);

        return gameState;
    }

    public GameState doAction(Action action, GameState gameState, String username, Game game, Turn turn) throws ActionDelegatingException {
        System.out.println("going to delegate action : " + action.getType());
        GameState gameState1 = actionDelegator.delegateAction(action, gameState);

        turn.getActions().add(action);

        logger.info("Actionmanager saves game for user: " + username);

        gameService.saveGame(game);

        System.out.println("TURN NUMBER :" + turn.getTurnNumber() + " ACTION COUNTER : " + gameState.getActionCounter() + ". action performed by : " + username);
        return gameState1;
    }

    public Turn createOrGetTurn(String username, Game game, GameState gameState) {
        game.setCurrentGameState(gameState);
        PlayerState playerWithTurn = gameState.getPlayerWithTurn();
        Player p = game.getPlayers().stream().filter(x -> x.getUser().getUsername().equals(playerWithTurn.getUsername())).findFirst().get();

        if (p.getTurnsPlayed().size() - 1  != gameState.getTurnCounter() ) {
            game.addTurnForPlayer(p.getUser().getUsername());
                System.out.println("action counter op -1 geset in de createOrGet Turn");
                gameState.setActionCounter(0);
        }

        Turn turn = game.getPlayers().stream().filter(x -> x.getUser().getUsername().equals(p.getUser().getUsername())).findFirst().get().getTurnsPlayed().get(p.getTurnsPlayed().size() - 1);

        return turn;
    }

    private Action parseAction(ActionDTO actionDTO, Turn turn) throws ParseActionException {
        try {
            ActionData actionData = actionParser.getActionData(actionDTO);
            ActionType actionType = actionParser.getActionType(actionDTO);

            return actionFactory.createAction(actionData, actionType, turn);

        } catch (InvalidActionDataException | ActionNotFoundException e) {
            throw new ParseActionException(e.getMessage());
        }
    }
}
