package be.kdg.catangamelogic.game_logic.action.exception;

public class InvalidBuildingSpotException extends Throwable {
    public InvalidBuildingSpotException(String message) {
        super(message);
    }
}
