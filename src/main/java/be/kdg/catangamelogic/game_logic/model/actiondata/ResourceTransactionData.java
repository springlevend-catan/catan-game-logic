package be.kdg.catangamelogic.game_logic.model.actiondata;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResourceTransactionData implements ActionData {
    @JsonProperty("playerFrom")
    private String playerFrom;
    @JsonProperty("playerTo")
    private String playerTo;
    @JsonProperty("receivedResources")
    private int[] receivedResources;
    @JsonProperty("removedResources")
    private int[] removedResources;
}
