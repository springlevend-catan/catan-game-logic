package be.kdg.catangamelogic.game_logic.model.state;

import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.action.CardType;
import be.kdg.catangamelogic.model.tile.TileType;
import be.kdg.catangamelogic.shared.exception.NotEnoughResourcesException;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class PlayerState implements Comparable<PlayerState> {

    private String username;

    private int points;

    private int longestRoad;

    private int numberOfKnights;

    private int turnOrder;

    private int colorId;

    private Map<TileType, Integer> resources;

    private Map<CardType, Integer> developmentCards;

    private boolean isActive;
    private boolean isHost;
    private boolean isComputer;

    public PlayerState(Player player) {
        this.username = player.getUser().getUsername();
        this.points = 0;
        this.longestRoad = 0;
        this.numberOfKnights = 0;
        this.turnOrder = player.getTurnOrder();
        this.colorId = player.getTurnOrder();
        this.isActive = true;
        this.isHost = player.isHost();
        this.initializeResources();
        this.isComputer = player.isComputer();
        this.initializeCards();
    }

    private void initializeResources() {
        resources = new HashMap<>();
        resources.put(TileType.BRICK, 0);
        resources.put(TileType.GRAIN, 0);
        resources.put(TileType.ORE, 0);
        resources.put(TileType.WOOD, 0);
        resources.put(TileType.WOOL, 0);
    }

    private void initializeCards() {
        developmentCards = new HashMap<>();
        developmentCards.put(CardType.KNIGHT, 0);
        developmentCards.put(CardType.VICTORY, 0);
        developmentCards.put(CardType.INVENTION, 0);
        developmentCards.put(CardType.STREET_BUILDING, 0);
        developmentCards.put(CardType.YEAR_OF_PLENTY, 0);
    }

    @Override
    public int compareTo(PlayerState o) {
        return Integer.compare(this.turnOrder, o.turnOrder);
    }

    public void removeResources(Map<TileType, Integer> removedResources) throws NotEnoughResourcesException {
        for (TileType tileType : removedResources.keySet()) {
            int newAmount = this.resources.get(tileType) - removedResources.get(tileType);
            if (newAmount >= 0) {
                this.resources.put(tileType, newAmount);
            } else {
                throw new NotEnoughResourcesException("Not enough resourcesToGive of type " + tileType + " expected " + removedResources.get(tileType) + " but got " + this.resources.get(tileType));
            }
        }
    }

    public void addResources(Map<TileType, Integer> receivedResources) {
        for (TileType tileType : receivedResources.keySet()) {
            this.resources.put(tileType, this.resources.get(tileType) + receivedResources.get(tileType));
        }
    }

    public void removeCard(CardType cardType){
        int previousValue = developmentCards.get(cardType);
        if(previousValue > 0){
            developmentCards.put(cardType,  previousValue - 1);
        }else{
            developmentCards.put(cardType, 0);
        }
    }

    public void addCard(CardType cardType){
        int previousValue = developmentCards.get(cardType);
        developmentCards.put(cardType, previousValue + 1);
    }

}
