package be.kdg.catangamelogic.game_logic.model.actiondata;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RobberStealResourceData implements ActionData {
    @JsonInclude(JsonInclude.Include.ALWAYS)
    private String takeFrom;
    private String giveTo;
}
