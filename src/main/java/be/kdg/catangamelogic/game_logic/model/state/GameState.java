package be.kdg.catangamelogic.game_logic.model.state;

import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.action.util.HexUtil;
import be.kdg.catangamelogic.game_logic.exception.InvalidBuildingTypeException;
import be.kdg.catangamelogic.game_logic.exception.InvalidIndexException;
import be.kdg.catangamelogic.game_logic.exception.NoDesertTileException;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.action.ActionType;
import be.kdg.catangamelogic.model.action.BuildAction;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.model.tile.Tile;
import be.kdg.catangamelogic.model.tile.TileType;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.aspectj.apache.bcel.util.Play;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Contains everything to visualize the game
 */
@Data
@NoArgsConstructor
public class GameState {

    private String url;
    @Getter(onMethod = @__( @JsonIgnore ))
    @JsonProperty("tiles")
    private List<List<TileState>> tileStates;
    @Getter(onMethod = @__( @JsonIgnore ))
    @JsonProperty("players")
    private List<PlayerState> playerStates;

    private ActionType requiredAction;

    private Action lastAction;

    /**
     * counter of how many actions have been performed this turn. Important for the replay of the game
     */
    private int actionCounter;

    private PlayerState playerWithTurn;

    private int[] robber;

    private int[] dice;

    private int turnCounter;

    private boolean isCompleted;


    public GameState(String url, List<Tile> tiles,List<Player> players) throws NoDesertTileException {
        this.url = url;
        this.actionCounter = 0;
        this.tileStates = parseBoard(tiles);
        this.playerStates =  new ArrayList<>();
        for (Player player : players) {
            this.playerStates.add(new PlayerState(player));
        }
        this.requiredAction = ActionType.BUILD_VILLAGE;
        this.playerStates.stream().filter(PlayerState::isHost).findFirst().ifPresent(playerState -> {
            this.setPlayerWithTurn(playerState);
        });
        Tile desertTile = tiles.stream().filter(t -> t.getTileType().equals(TileType.DESERT)).findFirst().orElseThrow(NoDesertTileException::new);

        this.robber = new int[]{desertTile.getCoordinateX(), desertTile.getCoordinateY()};
        this.turnCounter = 0;
        this.isCompleted = false;
    }

    public GameState(Game game) throws NoDesertTileException {
        this.url = game.getUrl();

        List<Tile> boardTiles = game.getTiles();

        this.requiredAction = ActionType.BUILD_VILLAGE;

        this.playerStates = new ArrayList<>();

        for (Player player : game.getPlayers()) {
            this.playerStates.add(new PlayerState(player));
        }

        this.playerStates.stream().filter(PlayerState::isHost).findFirst().ifPresent(playerState -> {
            this.setPlayerWithTurn(playerState);
        });

        Tile desertTile = boardTiles.stream().filter(t -> t.getTileType().equals(TileType.DESERT)).findFirst().orElseThrow(NoDesertTileException::new);

        this.robber = new int[]{desertTile.getCoordinateX(), desertTile.getCoordinateY()};

        this.tileStates = parseBoard(game.getTiles());

        this.turnCounter = 0;

        this.isCompleted = false;
    }

    private List<List<TileState>> parseBoard(List<Tile> tiles) {
        tiles.sort(Tile::compareTo);

        List<List<TileState>> rows = new ArrayList<>();
        List<TileState> row = new ArrayList<>();

        for (Tile tile : tiles) {
            if (tile.getCoordinateY() == 0) {
                row = new ArrayList<>();
                rows.add(row);
            }
            TileState tileState = new TileState(tile);
            row.add(tileState);
        }
        return rows;
    }

    public List<TileState> getTiles() {
        List<TileState> tiles = new ArrayList<>();
        getTileStates().forEach(tiles::addAll);
        return tiles;
    }

    public Optional<PlayerState> getPlayerStateByUsername(String username) {
        return this.playerStates.stream().filter(p -> p.getUsername().equals(username)).findFirst();
    }

    private PlayerState getNextPlayer() throws PlayerNotInGameException {
        playerStates.sort(PlayerState::compareTo);

        PlayerState playerState = playerStates.stream().filter(p -> p.getUsername().equals(playerWithTurn.getUsername())).findFirst()
                .orElseThrow(() -> new PlayerNotInGameException("No player with username " + playerWithTurn.getUsername() + " found"));

        PlayerState maxTurnOrderPlayer = playerStates.stream().max(PlayerState::compareTo).get();

        // find the player with the next turnOrder
        if (this.playerStates.size() == 1) {
            turnCounter++;

            return playerWithTurn;
        }
        if (playerState.getTurnOrder() != maxTurnOrderPlayer.getTurnOrder()) {
            if (turnCounter == 1) {
                if (playerWithTurn.getTurnOrder() == 0) {
                    turnCounter++;
                    return playerWithTurn;
                } else {

                    return playerStates.stream().filter(p -> p.getTurnOrder() == playerState.getTurnOrder() - 1).findFirst()
                            .orElseThrow(() -> new PlayerNotInGameException("No player with next turnOrder " + playerState.getTurnOrder() + 1 + " found"));
                }
            }
            return playerStates.stream().filter(p -> p.getTurnOrder() == playerState.getTurnOrder() + 1).findFirst()
                    .orElseThrow(() -> new PlayerNotInGameException("No player with next turnOrder " + playerState.getTurnOrder() + 1 + " found"));
        }
        // if this player is the last Player, return the first player
        else {
            if (turnCounter == 0) {
                turnCounter++;


                return playerWithTurn;
            } else if (turnCounter == 1) {
                return playerStates.stream().filter(p -> p.getTurnOrder() == playerState.getTurnOrder() - 1).findFirst()
                        .orElseThrow(() -> new PlayerNotInGameException("No player with next turnOrder " + playerState.getTurnOrder() + 1 + " found"));
            } else {
                turnCounter++;


                return playerStates.stream().filter(p -> p.getTurnOrder() == 0).findFirst().orElseThrow(() -> new PlayerNotInGameException("No player with turnOrder 0"));
            }
        }
    }

    public List<Building> getBuildings() {
        List<Building> buildings = new ArrayList<>();
        this.tileStates.forEach(tileStates1 -> tileStates1.forEach(tileState -> buildings.addAll(tileState.getBuildings())));
        return buildings;
    }

    public Optional<TileState> findTile(int x, int y) throws TileNotFoundException {
        return this.getTiles().stream().filter(t -> t.getX() == x && t.getY() == y).findFirst();
    }

    // TODO: Calculating route
    public void calculateLongestRoute(String username) throws PlayerNotInGameException, InvalidIndexException {
        List<Building> buildings = this.getStreetsByUsername(username);

    }

    // Gets all the street of one player
    private List<Building> getStreetsByUsername(String username) {
        List<Building> buildings = new ArrayList<>();
        for (Building b : this.getBuildings()) {
            if (b.getBuildingType() == BuildingType.STREET && b.getUsername().equals(username)) {
                buildings.add(b);
            }
        }
        return buildings;
    }

    public void endTurn() throws PlayerNotInGameException {
        this.playerWithTurn = (this.getNextPlayer());
    }

    // Checks if the building spot is valid
    public boolean isAvailableStreetSpot(int x, int y, int index, String username) throws TileNotFoundException {
        // Normal turn: build next to another street and not on another street
        if (this.turnCounter > 1) {
            return checkStreetSpot(x, y, index, username);
        }
        // First turns: build next to a village without a street
        else {
            HashMap<Integer, TileState> ns1 = getNeighboursWithIndex(x, y, index);
            if (streetHasVillageNeighbour(username, ns1)) return true;

            HashMap<Integer, TileState> ns2 = getNeighboursWithIndex(x, y, HexUtil.getNeighbouringIndeces(index)[0]);
            if (streetHasVillageNeighbour(username, ns2)) return true;
        }
        return false;
    }

    // checks if the street has a village without a street next to it
    private boolean streetHasVillageNeighbour(String username, HashMap<Integer, TileState> ns2) throws TileNotFoundException {
        if (this.turnCounter < 2) {
            for (Integer index : ns2.keySet()) {
                TileState n = ns2.get(index);
                if (n.getBuildings().stream().anyMatch(b ->
                        (b.getUsername().equals(username) &&
                                b.getBuildingType() == BuildingType.STREET
                                && (b.getIndex() == index || HexUtil.getNeighbouringIndeces(index)[1] == b.getIndex())))) {
                    return false;
                }
            }
        }
        for (Integer integer : ns2.keySet()) {
            TileState n = ns2.get(integer);
            if (n.getBuildings().stream().anyMatch(b -> b.getUsername().equals(username)
                    && b.getBuildingType() == BuildingType.VILLAGE
                    && b.getIndex() == integer)) {
                return true;
            }
        }
        return false;
    }

    // Checks if it is a valid street_building_spot: checks the current spot, checks if there is a street neighbour
    private boolean checkStreetSpot(int x, int y, int index, String username) throws TileNotFoundException {
        TileState t = findTile(x, y).orElseThrow(TileNotFoundException::new);
        boolean hasNeighbouringStreet = false;
        int compI = HexUtil.getComplementaryIndex(index).get();

        //Check neighbours of own tiles
        if (checkNeighbouringStreetSpots(x, y, index, username)) {
            hasNeighbouringStreet = true;
        }
        // Check the given spot of own tile
        if (t.getBuildings().stream().anyMatch(b -> b.getIndex() == index && b.getBuildingType() == BuildingType.STREET)) {
            return false;
        }

        // Do the same for the neighbour
        Optional<TileState> optionalNeighbour = getNeighbour(x, y, index);
        if (optionalNeighbour.isPresent()) {
            TileState n = optionalNeighbour.get();
            //Check neighbours of own tiles
            if (checkNeighbouringStreetSpots(n.getX(), n.getY(), compI, username)) {
                hasNeighbouringStreet = true;
            }
            // Check the given spot of neighbour tile
            if (n.getBuildings().stream().anyMatch(b -> b.getIndex() == compI && b.getBuildingType() == BuildingType.STREET)) {
                return false;
            }
        }
        return hasNeighbouringStreet;
    }

    // Checks the correct spots for a neighbouring street
    private boolean checkNeighbouringStreetSpots(int x, int y, int index, String username) throws TileNotFoundException {
        TileState tile = this.findTile(x, y).get();
        int[] ni = HexUtil.getNeighbouringIndeces(index);

        if (tile.getBuildings().stream().anyMatch(b -> b.getUsername().equals(username) && b.getBuildingType() == BuildingType.STREET && (b.getIndex() == ni[0] || b.getIndex() == ni[1]))
                || checkComplementaryStreet(x, y, ni[0], username)
                || checkComplementaryStreet(x, y, ni[1], username)) {
            return true;
        }

        return false;
    }

    // Checks the correct neighbour with the correct index
    private boolean checkComplementaryStreet(int x, int y, int index, String username) throws TileNotFoundException {
        int i1 = HexUtil.getComplementaryIndex(index).get();
        Optional<TileState> optionalN1 = this.getNeighbour(x, y, index);

        if (optionalN1.isPresent()) {
            TileState n1 = optionalN1.get();
            if (n1.getBuildings().stream().anyMatch(b -> b.getBuildingType() == BuildingType.STREET && b.getIndex() == i1 && b.getUsername().equals(username))) {
                return true;
            }
        }
        return false;
    }

    // checks if the spot has a street next to it
    private boolean hasStreetNeighbour(int x, int y, int index, String username) throws TileNotFoundException {
        if (turnCounter <= 1) {
            return true;
        }
        Optional<TileState> tileOptional = this.findTile(x, y);
        if (tileOptional.isPresent()) {
            TileState tile = tileOptional.get();
            if (!tile.getTileType().isWater()) {
                if (this.turnCounter >= 1) {
                    if (tile.getBuildings().stream().anyMatch(b ->
                            (b.getUsername().equals(username) &&
                                    b.getBuildingType() == BuildingType.STREET
                                    && (b.getIndex() == index || HexUtil.getNeighbouringIndeces(index)[1] == b.getIndex())))) {
                        return true;
                    }
                }
            } else {
                return false;
            }
        }
        return false;
    }

    // Checks to see if the spots around the index are empty and don't have a village/city too close and if there is a street next to it
    public boolean isAvailableVillageSpot(int x, int y, int index, String username) throws TileNotFoundException {
        // Check Neighbouring tiles for their complementary indeces;
        boolean hasStreet = false;

        // Find the tile of the neighbour and check it isn't water
        if (this.findTile(x, y).get().getTileType().isWater()) {
            return false;
        }

        // Get all the neighbouring tiles with the index of the village spot
        HashMap<Integer, TileState> neighbours = this.getNeighboursWithIndex(x, y, index);

        for (Integer i : neighbours.keySet()) {
            TileState t = neighbours.get(i);

            // Check if there is a village on the spot
            if (!checkVillageSpot(t.getX(), t.getY(), i, username)) {
                return false;
            }
            // Check if there is a village within 1 distance of the spot
            else if (!checkDistance(t.getX(), t.getY(), i, username)) {
                return false;
            }
            // Check if there is a street next to the spot
            else {
                if (hasStreetNeighbour(t.getX(), t.getY(), i, username)) {
                    hasStreet = true;
                }
            }
        }
        return hasStreet;
    }

    // checks if there is a village/city on the spot
    private boolean checkVillageSpot(int x, int y, int index, String username) throws TileNotFoundException {
        Optional<TileState> tileOptional = this.findTile(x, y);

        // Check if here is a village or city on the spot
        if (tileOptional.isPresent()) {
            TileState tile = tileOptional.get();
            return tile.getBuildings().stream().noneMatch(b ->
                    (b.getBuildingType() == BuildingType.VILLAGE || b.getBuildingType() == BuildingType.CITY) && b.getIndex() == index);
        }
        return true;
    }

    // Checks on index to the left and the right for the spots
    public boolean checkDistance(int x, int y, int index, String username) throws TileNotFoundException {
        int indexLeft = HexUtil.getNeighbouringIndeces(index)[0];
        int indexRight = HexUtil.getNeighbouringIndeces(index)[1];

        if (!checkVillageSpotsNeighbours(username, x, y, indexLeft)) {
            return false;
        } else return checkVillageSpotsNeighbours(username, x, y, indexRight);
    }

    // Checks all the neighbours of the village spot
    private boolean checkVillageSpotsNeighbours(String username, int x, int y, int index) throws TileNotFoundException {
        HashMap<Integer, TileState> neighbours = (getNeighboursWithIndex(x, y, index));
        for (Integer i : neighbours.keySet()) {
            TileState t = neighbours.get(i);
            if (!checkVillageSpot(t.getX(), t.getY(), i, username)) {
                return false;
            }
        }
        return true;
    }

    // Returns a board with the correct building_spots
    public List<List<TileState>> getAvailableSpots(String username, BuildingType buildingType) throws InvalidBuildingTypeException, TileNotFoundException {
        if (buildingType == BuildingType.VILLAGE_BUILDING_SPOT) {
            return getAvailableVillageSpots(username);
        } else if (buildingType == BuildingType.STREET_BUILDING_SPOT) {
            return getAvailableStreetSpots(username);
        } else {
            throw new InvalidBuildingTypeException();
        }
    }

    // Returns a board with street_building_spots
    private List<List<TileState>> getAvailableStreetSpots(String username) throws TileNotFoundException {
        List<List<TileState>> tiles = this.copyTiles();

        // Every tile can be build on without restrictions
        for (List<TileState> row : tiles) {
            for (TileState tile : row) {
                for (int i = 0; i < 6; i++) {
                    if (isAvailableStreetSpot(tile.getX(), tile.getY(), i, username)) {
                        tile.addBuilding(new Building(tile, i, BuildingType.STREET_BUILDING_SPOT, null, null, username, this.getPlayerStateByUsername(username).get().getTurnOrder()));
                    }
                    //tile.addBuilding(new Building(tile, i, BuildingType.VILLAGE_BUILDING_SPOT, null, null, username, this.getPlayerStateByUsername(username).get().getTurnOrder()));
                }
            }
        }

        return tiles;

    }

    // Returns a board with village_building_spots
    private List<List<TileState>> getAvailableVillageSpots(String username) throws TileNotFoundException {
        List<List<TileState>> tiles = this.copyTiles();

        // Every tile can be build on without restrictions
        for (List<TileState> row : tiles) {
            for (TileState tile : row) {
                for (int i = 0; i < 6; i++) {
                    if (isAvailableVillageSpot(tile.getX(), tile.getY(), i, username)) {
                        tile.addBuilding(new Building(tile, i, BuildingType.VILLAGE_BUILDING_SPOT, null, null, username, this.getPlayerStateByUsername(username).get().getTurnOrder()));
                    }
                    //tile.addBuilding(new Building(tile, i, BuildingType.VILLAGE_BUILDING_SPOT, null, null, username, this.getPlayerStateByUsername(username).get().getTurnOrder()));
                }
            }
        }

        return tiles;
    }

    // Calculating neighbours of a village
    public List<TileState> getNeighbours(int x, int y, int index) throws TileNotFoundException {
        List<TileState> neigbours = new ArrayList<>();

        // Add the tile passed with the parameters
        this.findTile(x, y).ifPresent(neigbours::add);

        // TopLeft is a neighbour
        if (index == 0 || index == 1) {
            getNeighbour(x, y, 0).ifPresent(neigbours::add);
        }
        // Top is a neighbour
        if (index == 1 || index == 2) {
            getNeighbour(x, y, 1).ifPresent(neigbours::add);
        }
        // TopRight is a neighbour
        if (index == 2 || index == 3) {
            getNeighbour(x, y, 2).ifPresent(neigbours::add);
        }
        // BottomRight is a neighbour
        if (index == 3 || index == 4) {
            getNeighbour(x, y, 3).ifPresent(neigbours::add);
        }
        // Bottom is a neighbour
        if (index == 4 || index == 5) {
            getNeighbour(x, y, 4).ifPresent(neigbours::add);
        }
        //BottomLeft is a neighbour
        if (index == 5 || index == 0) {
            getNeighbour(x, y, 5).ifPresent(neigbours::add);
        }
        return neigbours;
    }

    // Calculates neighbour of a street
    public Optional<TileState> getNeighbour(int x, int y, int i) throws TileNotFoundException {
        int xNeighbour;
        int yNeighbour;
        switch (i) {
            // Find TopLeft tile
            case 0:
                xNeighbour = x - 1;
                yNeighbour = y - 1;
                if (x > (this.getTileStates().size() - 1) / 2) {
                    yNeighbour++;
                }
                return this.findTile(xNeighbour, yNeighbour);
            // Find Top tile
            case 1:
                xNeighbour = x - 1;
                yNeighbour = y;
                if (x > this.getTileStates().size() / 2) {
                    yNeighbour++;
                }
                return this.findTile(xNeighbour, yNeighbour);
            // Find TopRight tile
            case 2:
                xNeighbour = x;
                yNeighbour = y + 1;
                return this.findTile(xNeighbour, yNeighbour);
            // Find BottomRight tile
            case 3:
                xNeighbour = x + 1;
                yNeighbour = y + 1;
                if (x >= (this.getTileStates().size() - 1) / 2) {
                    yNeighbour--;
                }
                return this.findTile(xNeighbour, yNeighbour);
            // Find Bottom tile
            case 4:
                xNeighbour = x + 1;
                yNeighbour = y;
                if (x >= (this.getTileStates().size()-1) / 2) {
                    yNeighbour--;
                }
                return this.findTile(xNeighbour, yNeighbour);
            // Find BottomLeft tile
            case 5:
                xNeighbour = x;
                yNeighbour = y - 1;
                return this.findTile(xNeighbour, yNeighbour);
            // Not a valid index
            default:
                return Optional.empty();
        }
    }

    // Calculates the neighbours of a village with the correct index;
    public HashMap<Integer, TileState> getNeighboursWithIndex(int x, int y, int index) throws TileNotFoundException {
        // Finds the neighbours and the corresponding indeces
        HashMap<Integer, TileState> neighbours = new HashMap<>();
        int indexRight;
        int indexLeft;

        if (index == 2 || index == 3) {
            indexRight = Arrays.stream(HexUtil.getNeighbouringVillageIndeces(index)).min().getAsInt();
            indexLeft = Arrays.stream(HexUtil.getNeighbouringVillageIndeces(index)).max().getAsInt();
        } else {
            indexRight = Arrays.stream(HexUtil.getNeighbouringVillageIndeces(index)).max().getAsInt();
            indexLeft = Arrays.stream(HexUtil.getNeighbouringVillageIndeces(index)).min().getAsInt();
        }

        Optional<TileState> nLeft = this.getNeighbour(x, y, HexUtil.getNeighbouringIndeces(index)[1]);
        Optional<TileState> nRight = this.getNeighbour(x, y, index);

        nRight.ifPresent(t -> neighbours.put(indexRight, t));
        nLeft.ifPresent(t -> neighbours.put(indexLeft, t));

        this.findTile(x, y).ifPresent(t -> neighbours.put(index, t));

        return neighbours;
    }

    // Returns a copy of the board to prevent call by reference
    private List<List<TileState>> copyTiles() {
        List<List<TileState>> tiles = new ArrayList<>();

        for (List<TileState> listTileState : this.getTileStates()) {
            List<TileState> row = new ArrayList<>();
            for (TileState tileState : listTileState) {
                row.add(new TileState(tileState));
            }
            tiles.add(row);
        }
        return tiles;
    }

    public Set<String> getStealPlayers() throws TileNotFoundException {
        Set<String> usernames = new HashSet<>();
        TileState tileState = findTile(robber[0], robber[1]).get();
        for (Building building : tileState.getBuildings()) {
            usernames.add(building.getUsername());
        }
        for (int i = 0; i < 6; i++) {
            Map<Integer, TileState> neighbours = getNeighboursWithIndex(robber[0], robber[1], i);
            for (Integer index : neighbours.keySet()) {
                Optional<Building> buildingOptional = neighbours.get(index).getBuildings().stream().filter(b->b.getIndex() == index).findFirst();
                if (buildingOptional.isPresent()) {
                    Building building = buildingOptional.get();
                    usernames.add(building.getUsername());
                }
            }
        }
        return usernames;
    }


}
