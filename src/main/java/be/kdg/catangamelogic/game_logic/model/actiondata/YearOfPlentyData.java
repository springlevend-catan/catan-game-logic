package be.kdg.catangamelogic.game_logic.model.actiondata;

import be.kdg.catangamelogic.model.tile.TileType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class YearOfPlentyData implements ActionData {
    private String resourceToGet;
}
