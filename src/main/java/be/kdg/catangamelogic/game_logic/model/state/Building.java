package be.kdg.catangamelogic.game_logic.model.state;

import be.kdg.catangamelogic.game_logic.action.util.HexUtil;
import be.kdg.catangamelogic.game_logic.exception.InvalidIndexException;
import be.kdg.catangamelogic.model.tile.TileType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.swing.text.html.Option;
import java.util.*;
import java.util.stream.DoubleStream;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Building {

    @JsonBackReference
    private TileState topLeftTile;

    private int index;

    private BuildingType buildingType;

    private List<TileState> neighbours;

    @JsonIgnore
    private Map<TileType, Integer> price;

    private String username;

    private int playerOrder;

    public boolean isNotAStreet() {
        return buildingType != BuildingType.STREET;
    }

    @JsonIgnore
    public List<TileState> getAllTiles() {
        List<TileState> allTiles = new ArrayList<>(neighbours);
        allTiles.add(topLeftTile);
        return allTiles;
    }

    public boolean neighboursTileWithNumber(int number) {
        return this.getAllTiles().stream().anyMatch(t -> t.getNumber() == number);
    }

    public Optional<Map<TileType, Integer>> getResources(int number) {
        Map<TileType, Integer> resources = new HashMap<>();
        if (this.isNotAStreet()) {
            int numberOfResources = this.buildingType == BuildingType.VILLAGE ? 1 : 2;
            this.getAllTiles().forEach(t ->
            {
                if (t.getNumber() == number && t.getTileType().isResource()) {
                    resources.putIfAbsent(t.getTileType(), 0);
                    resources.merge(t.getTileType(), numberOfResources, Integer::sum);
                }
            });
            return Optional.of(resources);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Building building = (Building) o;
        return index == building.index &&
                playerOrder == building.playerOrder &&
                buildingType == building.buildingType &&
                Objects.equals(username, building.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, buildingType, username, playerOrder);
    }
}
