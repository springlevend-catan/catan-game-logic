package be.kdg.catangamelogic.game_logic.model.state;

import be.kdg.catangamelogic.model.tile.TileType;
import be.kdg.catangamelogic.model.tile.Tile;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@NoArgsConstructor
public class TileState {

    private int x;

    private int y;

    private TileType tileType;

    private int number;
    @JsonManagedReference
    private List<Building> buildings;

    public TileState(Tile tile) {
        this.buildings = new ArrayList<>();
        this.x = tile.getCoordinateX();
        this.y = tile.getCoordinateY();
        this.tileType = tile.getTileType();
        this.number = tile.getNumber();
    }

    public TileState(TileState tileState){
        this.buildings = new ArrayList<>(tileState.buildings);
        this.x = tileState.getX();
        this.y = tileState.getY();
        this.tileType = tileState.getTileType();
        this.number = tileState.getNumber();
    }

    @Override
    public String toString() {
        return "TileState{" +
                "x=" + x +
                ", y=" + y +
                ", tileType=" + tileType +
                ", number=" + number +
                '}';
    }

    public void addBuilding(Building building){
        this.buildings.add(building);
    }

    public void removeBuildings(List<Building> buildings){
        for (Building b:buildings) {
            this.buildings.remove(b);
        }
    }


    public void removeBuilding(Building b) {
        this.buildings.remove(b);
    }
}
