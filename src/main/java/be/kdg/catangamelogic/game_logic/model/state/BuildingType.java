package be.kdg.catangamelogic.game_logic.model.state;

public enum BuildingType {
    VILLAGE, CITY, STREET, VILLAGE_BUILDING_SPOT, STREET_BUILDING_SPOT
}
