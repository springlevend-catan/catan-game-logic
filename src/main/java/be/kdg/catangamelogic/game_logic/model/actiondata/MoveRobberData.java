package be.kdg.catangamelogic.game_logic.model.actiondata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoveRobberData implements ActionData{
    private int x;
    private int y;
}
