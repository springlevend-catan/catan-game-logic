package be.kdg.catangamelogic.chat.model;

import java.util.ArrayList;
import java.util.List;

public class Chat {
    //@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private List<ChatMessage> messageList = new ArrayList<>();
    private List<String> connectedPlayers = new ArrayList<>();

    public Chat(String id) {
        this.id = id;
    }

    public Chat() {
    }

    public void sendMessage(ChatMessage chatMessage) {
        messageList.add(chatMessage);
    }

    public void addPlayer(String username) {
        if (connectedPlayers.contains(username)) return;

        connectedPlayers.add(username);
    }

    public void removePlayer(String username) {
        connectedPlayers.remove(username);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ChatMessage> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<ChatMessage> messageList) {
        this.messageList = messageList;
    }

    public List<String> getConnectedPlayers() {
        return connectedPlayers;
    }

    public void setConnectedPlayers(List<String> connectedPlayers) {
        this.connectedPlayers = connectedPlayers;
    }
}
