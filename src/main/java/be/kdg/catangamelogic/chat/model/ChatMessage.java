package be.kdg.catangamelogic.chat.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ChatMessage {
    private String id;
    private String username;
    private LocalDateTime time;
    private String message;

    public ChatMessage(String username, LocalDateTime time, String payload) {
        this.username = username;
        this.time = time;
        this.message = payload;
    }

    public ChatMessage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return username + " [" + DateTimeFormatter.ofPattern("HH:mm:ss").format(time) + "]: " + message;
    }
}
