package be.kdg.catangamelogic.chat;

import be.kdg.catangamelogic.chat.model.Chat;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ChatManager {
    private List<Chat> chatList = new ArrayList<>();

    public Chat getChat(String id) {
        for (Chat chat : chatList) {
            if (chat.getId().equals(id)) {
                return chat;
            }
        }
        return null;
    }

    public boolean addChat(Chat chat) {
        return chatList.add(chat);
    }
}
