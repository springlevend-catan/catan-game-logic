package be.kdg.catangamelogic.chat.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ChatMessageDto {
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private String sender;
    private String message;
    private String time;

    public ChatMessageDto() {
    }

    public ChatMessageDto(String sender, String message, LocalDateTime time) {
        this.sender = sender;
        this.message = message;

        setTime(time);
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = DATE_FORMAT.format(time);
    }
}
