package be.kdg.catangamelogic.chat.dto;

import java.util.List;

public class ChatDto {
    private String id;
    private List<String> players;

    public ChatDto() {
    }

    public ChatDto(String id, List<String> players) {
        this.id = id;
        this.players = players;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }
}
