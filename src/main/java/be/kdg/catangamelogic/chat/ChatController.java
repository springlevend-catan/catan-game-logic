package be.kdg.catangamelogic.chat;

import be.kdg.catangamelogic.chat.dto.ChatMessageDto;
import be.kdg.catangamelogic.chat.model.Chat;
import be.kdg.catangamelogic.chat.dto.ChatDto;
import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.shared.service.UserService;
import be.kdg.catangamelogic.shared.websocket.Connection;
import be.kdg.catangamelogic.shared.websocket.ConnectionManager;
import be.kdg.catangamelogic.shared.websocket.WebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.io.IOException;
import java.rmi.UnexpectedException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

// TODO: Combine lobby with chat
@Controller
public class ChatController {
    private final ChatManager chatManager;
    private final SimpMessagingTemplate messagingTemplate;
    private final ConnectionManager connectionManager;
    private final UserService userService;

    @Autowired
    public ChatController(ChatManager chatManager, SimpMessagingTemplate messagingTemplate, ConnectionManager connectionManager, UserService userService) {
        this.chatManager = chatManager;
        this.messagingTemplate = messagingTemplate;
        this.connectionManager = connectionManager;
        this.userService = userService;
    }

    @MessageMapping("/send-message/{token}/{id}")
    public void onReceiveMessage(@Payload String message, @DestinationVariable String token, @DestinationVariable String id) throws IOException {
        // Get chat
        Chat chat = chatManager.getChat(id);

        // If chat doesn't exist yet, something's gone wrong in onSubscribe, so throw Exception
        if (chat == null) {
            throw new UnexpectedException("Chat doesn't exist");
        }

        User user = userService.findExistingOrCreateUser(token);

        if (user == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        // Create new ChatDto; will contain info about the chat to sendPersonal in the response
        ChatDto chatDto = new ChatDto(chat.getId(), chat.getConnectedPlayers());

        // Create new ChatMessageDto; will contain message info
        ChatMessageDto chatMessageDto = new ChatMessageDto(user.getUsername(), message, LocalDateTime.now());

        // Response object (will become JSON)
        Map<String, Object> response = new HashMap<>();

        // Put DTOs in response
        response.put("chat", chatDto);
        response.put("message", chatMessageDto);

        // Path to sendPersonal to
        String path = String.format("/topic/chat/%s", id);

        // Send message to front-end chat
        messagingTemplate.convertAndSend(path, response);
    }

    /**
     * Method that is triggered when a player subscribes to a chat. Sends an initial 'x has joined chat y.' message.
     *
     * @param event The event that should trigger the method call
     */
    @EventListener
    public void onSubscribe(SessionSubscribeEvent event) {
        // Get ID of WebSocket session
        String sessionId = (String) event
                .getMessage()
                .getHeaders()
                .get("simpSessionId");

        // 1. Check for null or IntelliJ produces a warning
        // 2. Check if the ID of the WebSocket of the chat matches the ID of the WebSocket being subscribed to
        //
        // If so, leave method
        Connection connection = connectionManager.findBySessionId(sessionId);

        if (sessionId == null || connection == null || connection.getWebSocket() != WebSocket.CHAT) {
            return;
        }

        String webSocketId = connection.getWebSocketId();
        String username = connection.getUsername();

        // Get chat
        Chat chat = chatManager.getChat(webSocketId);

        // If chat doesn't exist yet, create it, and add it to chatManager
        if (chat == null) {
            chat = new Chat(webSocketId);

            chatManager.addChat(chat);
        }

        // If player is already connected, stop, because we've already sent a join message
        if (chat.getConnectedPlayers().contains(username)) {
            return;
        }

        // Add player to list of players connected to chat
        chat.addPlayer(username);

        // Create new ChatDto; will contain info about the chat, including ID and players, the latter of which will be
        // shown in the side bar of the chat so people know who's in the chat
        ChatDto chatDto = new ChatDto(chat.getId(), chat.getConnectedPlayers());

        // Response to sendPersonal (ends up as JSON)
        Map<String, Object> response = new HashMap<>();

        response.put("chat", chatDto);
        response.put("username", username);
        response.put("type", "join");

        // URL to sendPersonal initial join message to
        String path = String.format("/topic/chat/%s", chat.getId());

        // Send response to given URL
        messagingTemplate.convertAndSend(path, response);
    }

    /**
     * Method that is triggered when a player leaves the chat. Sends a 'x has left chat y.' message.
     *
     * @param event The event that should trigger the method call
     * @throws UnexpectedException Thrown if the chat doesn't exist, which means something's gone horribly wrong
     *                             considering the chat is made on the subscribe event if it didn't exist yet at that point
     */
    @EventListener
    public void onChatLeave(SessionDisconnectEvent event) throws UnexpectedException {
        String sessionId = event.getSessionId();

        Connection connection = connectionManager.findBySessionId(sessionId);

        // Check if the ID of the WebSocket of the chat matches the ID of the WebSocket being subscribed to
        if (connection == null) {
            throw new UnexpectedException("Connection is null/doesn't exist");
        }

        if (connection.getWebSocket() != WebSocket.CHAT) {
            return;
        }

        // Get chat
        Chat chat = chatManager.getChat(connection.getWebSocketId());

        // People can't leave a non-existent chat, so throw an exception if it doesn't exist
        if (chat == null) {
            throw new UnexpectedException("Chat room doesn't have matching object");
        }

        // Remove player from chat
        chat.removePlayer(connection.getUsername());

        // Create ChatDto; will hold info about the chat
        ChatDto chatDto = new ChatDto(chat.getId(), chat.getConnectedPlayers());

        // Create response (interpreted as JSON)
        Map<String, Object> response = new HashMap<>();

        // Add chat
        response.put("chat", chatDto);

        // Add username of leaving player
        response.put("username", connection.getUsername());

        // Set message type (needed for styling)
        response.put("type", "leave");

        // URL to sendPersonal to
        String path = String.format("/topic/chat/%s", chat.getId());

        // Send message to given URL
        messagingTemplate.convertAndSend(path, response);
    }
}
