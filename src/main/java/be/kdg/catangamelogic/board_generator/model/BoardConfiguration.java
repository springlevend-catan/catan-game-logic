package be.kdg.catangamelogic.board_generator.model;

import be.kdg.catangamelogic.model.tile.TileType;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class BoardConfiguration {

    private final static HashMap<TileType, Integer> defaultPorts = new HashMap<TileType, Integer>() {{
        put(TileType.PORT, 5);
        put(TileType.PORT_BRICK, 1);
        put(TileType.PORT_GRAIN, 1);
        put(TileType.PORT_ORE, 1);
        put(TileType.PORT_WOOD, 1);
        put(TileType.PORT_WOOL, 1);
    }};

    private final static HashMap<TileType, Integer> defaultResources = new HashMap<TileType, Integer>() {{
        put(TileType.WOOD, 4);
        put(TileType.GRAIN, 4);
        put(TileType.WOOL, 4);
        put(TileType.BRICK, 3);
        put(TileType.ORE, 3);
        put(TileType.DESERT, 1);
    }};

    public HashMap<TileType, Integer> getPorts(){
        return (HashMap<TileType, Integer>) defaultPorts.clone();
    }

    public HashMap<TileType, Integer> getResources(){
        return (HashMap<TileType, Integer>) defaultResources.clone();
    }
}

