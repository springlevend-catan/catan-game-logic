package be.kdg.catangamelogic.board_generator.model;

import be.kdg.catangamelogic.model.tile.TileType;

public class GeneratedTile {

    private TileType type;
    private int number;
    private int x;
    private int y;
    public GeneratedTile(TileType type, int number) {
        this.type = type;
        this.number = number;
    }

    public TileType getType() {
        return type;
    }

    public int getNumber() {
        return number;
    }
}
