package be.kdg.catangamelogic.board_generator.business;

import be.kdg.catangamelogic.board_generator.model.BoardConfiguration;
import be.kdg.catangamelogic.model.tile.TileType;
import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.model.tile.Tile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BoardGenerator {
    private final static int[] numberSequence = {4, 6, 9, 2, 5, 12, 4, 9, 8, 8, 10, 3, 5, 10, 11, 3, 6, 11};
    private final static int defaultSize = 7;
    private static Random generator;

    private static HashMap<TileType, Integer> ports;
    private static HashMap<TileType, Integer> resources;

    private static BoardConfiguration boardSettings;

    @Autowired
    public BoardGenerator(Random generator, BoardConfiguration boardSettings) {
        BoardGenerator.generator = generator;
        BoardGenerator.boardSettings = boardSettings;
    }

    public static List<Tile> generateBoard() throws TileNotFoundException {
        int size = defaultSize;

        List<TileType[]> boardTiles = new ArrayList<>();
        ports = boardSettings.getPorts();
        resources = boardSettings.getResources();

        TileType previousWater = null;
        TileType rightWater = null;


        int min_rows = (size - 1) / 2;

        for (int i = 0; i < size; i++) {
            int rowSize = size - Math.abs(min_rows - i);

            boardTiles.add(new TileType[rowSize]);

            for (int j = 0; j < rowSize; j++) {
                TileType type;
                //First row of alternating ports and water
                if (j == 0) {
                    if (i == 0) {
                        type = getRandomWithWeight(ports);
                        previousWater = type;
                    } else {
                        type = getWaterTile(previousWater);
                        previousWater = type;
                        rightWater = type;
                    }
                } else if (i == 0 || i == size - 1) {
                    type = getWaterTile(previousWater);
                    previousWater = type;
                } else if (j == rowSize - 1) {
                    type = getWaterTile(rightWater);
                    rightWater = type;
                } else {
                    type = getRandomWithWeight(resources);
                }

                if (type != null) {
                    boardTiles.get(i)[j] = type;
                }
            }
        }

        int resourceIndex = 0;

        List<Tile> tiles = new ArrayList<Tile>();


        for (int i = 0; i < boardTiles.size(); i++) {
            for (int j = 0; j < boardTiles.get(i).length; j++) {
                int number = 0;
                TileType type = boardTiles.get(i)[j];
                if (TileUtil.isResource(type)) {
                    number = numberSequence[resourceIndex];
                    resourceIndex++;
                }
                tiles.add(new Tile( i, j, type, number));
            }
        }

        return tiles;
    }

    private static TileType getWaterTile(TileType previousTile) throws TileNotFoundException {
        if (previousTile == TileType.WATER) {
            return getRandomWithWeight(ports);
        } else {
            return TileType.WATER;
        }
    }

    private static TileType getRandomWithWeight(Map<TileType, Integer> list) throws TileNotFoundException {
        int total = list.values().stream().reduce(0, (x, y) -> x + y);
        int value = generator.nextInt(total) + 1;
        int count = 0;

        for (Map.Entry<TileType, Integer> entry : list.entrySet()) {
            count += entry.getValue();
            if (count >= value) {
                entry.setValue(entry.getValue() - 1);
                return entry.getKey();
            }
        }

        throw new TileNotFoundException();
    }

}
