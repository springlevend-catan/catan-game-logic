package be.kdg.catangamelogic.board_generator.business;

import be.kdg.catangamelogic.model.tile.TileType;

public class TileUtil {
    public static boolean isResource(TileType type) {
        if (type == TileType.ORE || type == TileType.BRICK || type == TileType.GRAIN || type == TileType.WOOD || type == TileType.WOOL) {
            return true;
        } else {
            return false;
        }
    }
}
