package be.kdg.catangamelogic.game_session.game.repository;

import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.game.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.*;
public interface GameRepository extends JpaRepository<Game, Long> {

    Game findByUrl(String url);

    Game findFirstByUrl(String url);


    List<Game> findAllByPlayersContains(List<Player> players);
}
