package be.kdg.catangamelogic.game_session.game.manager;

import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.action.delegator.ActionDelegator;
import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.exception.NoDesertTileException;
import be.kdg.catangamelogic.game_logic.model.state.PlayerState;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.game_session.game.generator.GameGenerator;
import be.kdg.catangamelogic.game_session.exception.CreateGameException;
import be.kdg.catangamelogic.shared.service.PlayerService;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.shared.service.GameService;
import be.kdg.catangamelogic.game_session.lobby.model.Lobby;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.game.Game;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class GameStateManager {

    private static List<GameState> gameStates;

    private final GameService gameService;
    private ActionDelegator delegator;
    private final PlayerService playerService;
    private final GameGenerator gameGenerator;

    public GameStateManager(GameService gameService, ActionDelegator delegator, PlayerService playerService, GameGenerator gameGenerator) {
        this.gameService = gameService;
        this.delegator = delegator;
        this.playerService = playerService;
        this.gameGenerator = gameGenerator;
        gameStates = new LinkedList<>();
    }

    public String startGame(Lobby lobby) throws CreateGameException {

        Game game;
        try {
            game = gameGenerator.createGame(lobby);
        } catch (TileNotFoundException | NoDesertTileException | PlayerNotInGameException e) {
            throw new CreateGameException(e.getMessage());
        }

        Game savedgame = gameService.saveGame(game);

        gameStates.add(savedgame.getCurrentGameState());

        return savedgame.getUrl();
    }

    /**
     * Retrieves the current gameState of the game either by looking in the cache or calculating it from the actions
     *
     * @param url      The url of the game
     * @param username The username of the player
     * @return the current gameState of the game
     * @throws PlayerNotInGameException   thrown when the player isn't in the game
     * @throws GameNotCalculableException thrown when the game couldn't be retrieved
     */
    public GameState getGameState(String url, String username) throws PlayerNotInGameException, GameNotCalculableException {

        Game game = gameService.getGameByUrl(url);

        // Check if the user is in the game
        if (game.getPlayers().stream().anyMatch(p -> p.getUser().getUsername().equals(username))) {
            return getCurrentGameState(url);
        } else {
            throw new PlayerNotInGameException();
        }
    }

    private GameState getCurrentGameState(String url) throws GameNotCalculableException, PlayerNotInGameException {
        Optional<GameState> gameState = GameStateManager.gameStates.stream().filter(g -> g.getUrl().equals(url)).findFirst();
        if (gameState.isPresent()) {
            return gameState.get();
        }
        return calculateGameState(url);
    }

    private GameState calculateGameState(String url) throws GameNotCalculableException, PlayerNotInGameException {
        Game game = gameService.getGameByUrl(url);

        GameState gameState;
        try {
            gameState = new GameState(game);
        } catch (NoDesertTileException e) {
            throw new GameNotCalculableException();
        }

        List<Turn> turns = game.getSortedTurns();

        for (Turn turn : turns) {
            List<Action> actions = turn.getActions();
            actions.sort(Action::compareTo);
            for (Action action : actions) {
                try {
                    delegator.delegateAction(action, gameState);
                } catch (ActionDelegatingException e) {
                    throw new GameNotCalculableException();
                }
            }
        }
        return gameState;
    }

    /**
     * @param url      The url of the game
     * @param username The username of the player
     * @param isActive The state the player needs to be changed to
     * @return The updated GameState
     * @throws PlayerNotInGameException   is thrown when the player isn't part of the game
     * @throws GameNotCalculableException the game couldn't be retrieved, because of an invalid calculation
     */
    public GameState setIsActivePlayer(String url, String username, boolean isActive) throws PlayerNotInGameException, GameNotCalculableException {
        GameState gameState = getCurrentGameState(url);

        // Set the correct player to the correct active state
        gameState.getPlayerStates()
                .stream()
                .filter(player -> player.getUsername()
                        .equals(username))
                .findFirst().orElseThrow(PlayerNotInGameException::new).setActive(isActive);

        // If there are no more active players remove the gameState from memory
        if (!isActive && gameState.getPlayerStates().stream().filter(PlayerState::isActive).toArray().length == 0) {
            gameStates.remove(gameState);
        }

        return gameState;
    }
}
