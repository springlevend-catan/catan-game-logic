package be.kdg.catangamelogic.game_session.game.controller;


import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.exception.InvalidBuildingTypeException;
import be.kdg.catangamelogic.game_logic.exception.InvalidBuildingTypeException;
import be.kdg.catangamelogic.game_logic.model.state.Building;
import be.kdg.catangamelogic.game_logic.model.state.BuildingType;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.game_logic.model.state.TileState;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.game_session.game.manager.GameNotCalculableException;
import be.kdg.catangamelogic.game_session.game.manager.GameStateManager;
import be.kdg.catangamelogic.model.action.ActionType;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.service.GameService;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class GameRestController {

    private final GameService gameService;
    private final TokenConverter tokenConverter;
    private final GameStateManager gameStateManager;

    public GameRestController(GameService gameService, TokenConverter tokenConverter, GameStateManager gameStateManager) {
        this.gameService = gameService;
        this.tokenConverter = tokenConverter;
        this.gameStateManager = gameStateManager;
    }

    @GetMapping("/games")
    public ResponseEntity<List<Game>> getGames(@RequestHeader("Authorization") String token) {
        String username = tokenConverter.convertToken(token).getSubject();
        return new ResponseEntity<>(gameService.findByUser(username), HttpStatus.OK);
    }

    @GetMapping("/game/villageSpots/{url}")
    public ResponseEntity<List<List<TileState>>> getVillageBuildingSpots(@PathVariable("url") String url, @RequestHeader("Authorization") String token) throws GameNotCalculableException, PlayerNotInGameException, InvalidBuildingTypeException, TileNotFoundException {
        String username = tokenConverter.convertToken(token).getSubject();
        GameState gameState = gameStateManager.getGameState(url, username);
        List<List<TileState>> availableSpots = gameState.getAvailableSpots(username, BuildingType.VILLAGE_BUILDING_SPOT);
        return new ResponseEntity<>(availableSpots, HttpStatus.OK);
    }

    @GetMapping("/game/streetSpots/{url}")
    public ResponseEntity<List<List<TileState>>> getStreetBuildingSpots(@PathVariable("url") String url, @RequestHeader("Authorization") String token) throws GameNotCalculableException, PlayerNotInGameException, InvalidBuildingTypeException, TileNotFoundException {
        String username = tokenConverter.convertToken(token).getSubject();
        GameState gameState = gameStateManager.getGameState(url, username);
        List<List<TileState>> availableSpots = gameState.getAvailableSpots(username, BuildingType.STREET_BUILDING_SPOT);
        return new ResponseEntity<>(availableSpots, HttpStatus.OK);
    }

    // TODO: Return the possible actions of the player
    @GetMapping("/game/availableActions/{url}")
    public ResponseEntity<List<ActionType>> getAvailableActions(@PathVariable("url") String url, @RequestHeader("Authorization") String token){
        return null;
    }

    @GetMapping("game/steal/{url}")
    public ResponseEntity<String[]> getStealAvailablePlayers(@PathVariable("url") String url, @RequestHeader("Authorization") String token) throws GameNotCalculableException, PlayerNotInGameException, TileNotFoundException {
        String username = tokenConverter.convertToken(token).getSubject();
        GameState gameState = gameStateManager.getGameState(url, username);
        Set<String> playerSet = gameState.getStealPlayers();
        playerSet.remove(username);
        String[] players = playerSet.toArray(new String[playerSet.size()]);
        return new ResponseEntity<>(players, HttpStatus.OK);
    }
}
