package be.kdg.catangamelogic.game_session.game.model;

import be.kdg.catangamelogic.game_logic.model.state.*;
import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.action.ActionType;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class GameStateDTO {
    private String url;

    private List<List<TileState>> tiles;

    private List<PlayerState> players;

    private ActionType requiredAction;

    private int actionCounter;

    private PlayerState playerWithTurn;

    private int[] robber;

    private int[] dice;

    private int turnCounter;

    private boolean isCompleted;


    public GameStateDTO(GameState gameState) {
        this.url = gameState.getUrl();
        this.players = gameState.getPlayerStates();
        this.requiredAction = gameState.getRequiredAction();
        this.playerWithTurn = gameState.getPlayerWithTurn();
        this.tiles = gameState.getTileStates();
        this.turnCounter = gameState.getTurnCounter();
        this.robber = gameState.getRobber();
        this.dice = gameState.getDice();
        this.actionCounter = gameState.getActionCounter();
        this.isCompleted = gameState.isCompleted();
    }
}
