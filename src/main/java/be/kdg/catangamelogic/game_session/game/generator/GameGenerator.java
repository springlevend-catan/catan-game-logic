package be.kdg.catangamelogic.game_session.game.generator;

import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.exception.NoDesertTileException;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.shared.service.GameService;
import be.kdg.catangamelogic.game_session.lobby.model.Lobby;
import be.kdg.catangamelogic.shared.service.PlayerService;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.service.UserService;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class GameGenerator {

    private final GameService gameService;

    private final PlayerService playerService;

    private final UserService userService;

    public GameGenerator(GameService gameService, PlayerService playerService, UserService userService) {
        this.gameService = gameService;
        this.playerService = playerService;
        this.userService = userService;
    }

    public Game createGame(Lobby lobby) throws TileNotFoundException, UsernameNotFoundException, NoDesertTileException, PlayerNotInGameException {

        List<Player> players = new ArrayList<>();

        User computer1 = userService.findUserByUsername("Computer 1");
        User computer2 = userService.findUserByUsername("Computer 2");
        User computer3 = userService.findUserByUsername("Computer 3");

        if (computer1 == null){
            computer1 = new User("Computer 1", "computer@kolonisten-van-catan-ip2.com");
            userService.saveUser(computer1);
        }
        if(computer2 == null){
            computer2 = new User("Computer 2", "computer2@kolonisten-van-catan-ip2.com");
            userService.saveUser(computer2);
        }
        if(computer3 == null){
            computer3 = new User("Computer 3", "computer3@kolonisten-van-catan-ip2.com");
            userService.saveUser(computer3);
        }

        ArrayList<User> computerList = new ArrayList<>();
        computerList.add(computer1);
        computerList.add(computer2);
        computerList.add(computer3);
        final int[] computercounter = {0};


        // Array because Java is weird with lambda functions
        // (Variables in lambdas should be final, so a final array where you can still change the values within)
        final int[] turnOrder = {0};

        // Add the hostPlayer
        User hostUser = lobby.getHost();
        Player hostPlayer = new Player(hostUser, turnOrder[0], true);
        //hostPlayer.addTurn(new Turn(hostPlayer, 0));

        players.add(hostPlayer);
        // Add the other players
        lobby.getUsers().forEach(u -> {
            turnOrder[0]++;
            User user;
            Player player;
            if (u.getUsername().toLowerCase().startsWith("computer")){
                user = computerList.get(computercounter[0]);
                computercounter[0]++;
                player = new Player(user, turnOrder[0], false);
                player.setComputer(true);
            }else{
                user = (userService.findUserByUsername(u.getUsername()));
                player = new Player(user, turnOrder[0], false);
            }
            players.add(player);
        });

        Game game = new Game(players);

        return game;
    }
}
