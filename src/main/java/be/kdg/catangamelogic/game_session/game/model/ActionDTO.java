package be.kdg.catangamelogic.game_session.game.model;

import be.kdg.catangamelogic.model.Turn;
import be.kdg.catangamelogic.model.action.ActionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActionDTO {
    private String token;

    private String value;

    private String type;
}
