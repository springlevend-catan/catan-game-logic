package be.kdg.catangamelogic.game_session.game.controller;


import be.kdg.catangamelogic.board_generator.exception.TileNotFoundException;
import be.kdg.catangamelogic.game_logic.action.delegator.ComputerDelegator;
import be.kdg.catangamelogic.game_logic.action.exception.ActionDelegatingException;
import be.kdg.catangamelogic.game_logic.action.exception.NotYourTurnException;
import be.kdg.catangamelogic.game_logic.action.exception.ParseActionException;
import be.kdg.catangamelogic.game_logic.action.manager.ActionManager;
import be.kdg.catangamelogic.game_logic.exception.ActionNotFoundException;
import be.kdg.catangamelogic.game_logic.exception.InvalidBuildingTypeException;
import be.kdg.catangamelogic.game_logic.model.state.GameState;
import be.kdg.catangamelogic.game_session.game.exception.PlayerNotInGameException;
import be.kdg.catangamelogic.game_session.game.manager.GameNotCalculableException;
import be.kdg.catangamelogic.game_session.game.manager.GameStateManager;
import be.kdg.catangamelogic.game_session.game.model.ActionDTO;
import be.kdg.catangamelogic.game_session.game.model.GameStateDTO;
import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import org.modelmapper.ModelMapper;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class GameWebSocket {
    private final ModelMapper modelMapper;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final GameStateManager gameStateManager;
    private final TokenConverter tokenConverter;
    private final ActionManager actionManager;
    private final ComputerDelegator computerDelegator;


    public GameWebSocket(ModelMapper modelMapper, SimpMessagingTemplate simpMessagingTemplate, GameStateManager gameStateManager, TokenConverter tokenConverter, ActionManager actionManager, ComputerDelegator computerDelegator) {
        this.modelMapper = modelMapper;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.gameStateManager = gameStateManager;
        this.tokenConverter = tokenConverter;
        this.actionManager = actionManager;
        this.computerDelegator = computerDelegator;
    }

    // Receives actions and processes it on the correct gameState
    @MessageMapping("/game/action/{url}")
    public void doAction(@DestinationVariable("url") String url, ActionDTO actionDTO) throws PlayerNotInGameException, GameNotCalculableException, ParseActionException, ActionDelegatingException, NotYourTurnException {
        GameState gameState;
        gameState = actionManager.processAction(url, actionDTO);
        System.out.println("gameState gaat naar buiten : " + gameState.getActionCounter());
        GameStateDTO gameStateDTO = new GameStateDTO(gameState);
        simpMessagingTemplate.convertAndSend("/topic/game/" + url, gameStateDTO);
    }


    @MessageMapping("/game/playComputer/{url}")
    public void playComputer(@DestinationVariable("url") String url, String username) throws PlayerNotInGameException, GameNotCalculableException, ParseActionException, ActionDelegatingException, ActionNotFoundException, InvalidBuildingTypeException, TileNotFoundException {
        GameState computerGameState = computerDelegator.delegateComputer(url, username);
        GameStateDTO computerGameStateDTO = new GameStateDTO(computerGameState);
        simpMessagingTemplate.convertAndSend("/topic/game/" + url, computerGameStateDTO);
    }

    @MessageMapping("/game/join/{url}")
    public void joinGame(@DestinationVariable("url") String url, String token) throws
            PlayerNotInGameException, GameNotCalculableException {
        String username = tokenConverter.convertToken(token).getSubject();

        GameState gameState = gameStateManager.setIsActivePlayer(url, username, true);
        System.out.println("joined");
        GameStateDTO gameStateDTO = new GameStateDTO(gameState);
        simpMessagingTemplate.convertAndSend("/topic/game/" + url, gameStateDTO);
    }

    @MessageMapping("/game/leave/{url}")
    public void leaveGame(@DestinationVariable("url") String url, String token) throws
            PlayerNotInGameException, GameNotCalculableException {
        String username = tokenConverter.convertToken(token).getSubject();

        GameState gameState = gameStateManager.setIsActivePlayer(url, username, false);
        System.out.println("left");
        simpMessagingTemplate.convertAndSend("/topic/game/" + url, new GameStateDTO(gameState));
    }

}

