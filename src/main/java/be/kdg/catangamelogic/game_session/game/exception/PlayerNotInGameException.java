package be.kdg.catangamelogic.game_session.game.exception;

public class PlayerNotInGameException extends Throwable {
    public PlayerNotInGameException(String message) {
        super(message);
    }

    public PlayerNotInGameException() {
    }
}
