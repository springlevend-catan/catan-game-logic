package be.kdg.catangamelogic.game_session.exception;

public class CreateGameException extends Throwable {
    public CreateGameException(String message) {
        super(message);
    }

    public CreateGameException() {
    }
}
