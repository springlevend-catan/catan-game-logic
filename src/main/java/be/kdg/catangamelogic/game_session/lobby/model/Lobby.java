package be.kdg.catangamelogic.game_session.lobby.model;

import be.kdg.catangamelogic.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

@Data
@NoArgsConstructor
public class Lobby {
    private User host;
    private List<User> users;
    private String url;
    private int size;
    private boolean available;
    private boolean closed;
    private String gameUrl;

    public boolean isAvailable() {
        return users.size() <= size && available;
    }

    public Lobby(String url) {
        users = new LinkedList<>();
        this.url = url;
        this.available = true;
        this.gameUrl = null;
    }

    public List<User> getUsers() {
        return users;
    }

    public void joinPlayer(User user) {
        if (!this.users.contains(user) && this.available) {
            this.users.add(user);
        }
    }

    public void leaveUser(User user) {
        this.users.removeIf(p -> p.getUsername().equals(user.getUsername()));
    }
}