package be.kdg.catangamelogic.game_session.lobby.repositories;

import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.game.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;

public interface PlayerRepository extends JpaRepository<Player, Long> {
    public List<Player> findAllByGame(Game gameUrl);

    public List<Player> findAllByUser_Username(String usernam);
}
