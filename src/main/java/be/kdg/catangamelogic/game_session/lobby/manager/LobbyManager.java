package be.kdg.catangamelogic.game_session.lobby.manager;

import be.kdg.catangamelogic.game_session.lobby.exception.LobbyNotFoundException;
import be.kdg.catangamelogic.game_session.lobby.model.PlayerDTO;
import be.kdg.catangamelogic.game_session.lobby.model.Lobby;
import be.kdg.catangamelogic.model.User;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class LobbyManager {

    private List<Lobby> lobbies;

    static int counter = 0;
    private List<Lobby> lobbies1;

    public LobbyManager() {
        lobbies = new LinkedList<>();
    }

    public String createLobby(User host) {
        String url = String.valueOf(counter);

        Lobby lobby = new Lobby(url);
        lobby.setHost(host);
        lobby.setAvailable(true);
        lobby.setSize(4);
        this.lobbies.add(lobby);

        counter++;
        return url;
    }

    public Lobby joinLobby(String url, User user) throws LobbyNotFoundException {
        Optional<Lobby> optionalLobby = this.lobbies.stream().filter(l -> l.getUrl().equals(url)).findFirst();
        if (optionalLobby.isPresent()) {
            Lobby lobby = optionalLobby.get();
            // Host joins when the lobby is created, so doesn't join here
            if (!lobby.getHost().equals(user)){
                lobby.joinPlayer(user);
            }
            return lobby;
        }

        throw new LobbyNotFoundException("Lobby " + url + " not found");
    }

    public Lobby leavePlayer(String url, User user) throws LobbyNotFoundException {
        Optional<Lobby> optionalLobby = this.lobbies.stream().filter(l -> l.getUrl().equals(url)).findFirst();
        if (optionalLobby.isPresent()) {
            Lobby lobby = optionalLobby.get();
            if (lobby.getHost().getUsername().equals(user.getUsername())) {
                lobbies.remove(lobby);
            } else {
                lobby.leaveUser(user);
            }
            return lobby;
        }
        throw new LobbyNotFoundException("Lobby " + url + "not found");
    }

    public List<Lobby> getAvailableLobbies() {
        List<Lobby> lobbies =  this.lobbies.stream().filter(x -> x.isAvailable()).collect(Collectors.toList());
        return lobbies;
    }

    public void removeLobby(Lobby lobby) {
        this.lobbies.remove(lobby);
    }

    public Lobby getLobby(String url) {
        return this.lobbies.stream().filter(l -> l.getUrl().equals(url)).findFirst().get();
    }

    public Lobby addComputerPlayer(String url) throws LobbyNotFoundException {
        Optional<Lobby> optionalLobby = this.lobbies.stream().filter(l -> l.getUrl().equals(url)).findFirst();
        if (optionalLobby.isPresent()) {
            Lobby lobby = optionalLobby.get();
            int computerNumber = lobby.getUsers().size();
            User computer = new User("Computer" + computerNumber, null);
            lobby.joinPlayer(computer);

            return lobby;
        }

        throw new LobbyNotFoundException("Lobby " + url + " not found");
    }
}
