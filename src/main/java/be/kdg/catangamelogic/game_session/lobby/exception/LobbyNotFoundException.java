package be.kdg.catangamelogic.game_session.lobby.exception;

public class LobbyNotFoundException extends Throwable {
    public LobbyNotFoundException(String message) {
        super(message);
    }
}
