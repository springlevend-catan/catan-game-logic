package be.kdg.catangamelogic.game_session.lobby.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlayerDTO {

    private String email;
    private String username;
    private Boolean isHost;

    public PlayerDTO(String email, String username) {
        this.email = email;
        this.username = username;
        this.isHost=false;
    }

    public PlayerDTO(String email, String username, Boolean isHost) {
        this.email = email;
        this.username = username;
        this.isHost = isHost;
    }
}
