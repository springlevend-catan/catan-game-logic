package be.kdg.catangamelogic.game_session.lobby.controller;

import be.kdg.catangamelogic.game_session.game.manager.GameStateManager;
import be.kdg.catangamelogic.game_session.exception.CreateGameException;
import be.kdg.catangamelogic.game_session.lobby.model.Lobby;
import be.kdg.catangamelogic.game_session.lobby.manager.LobbyManager;
import be.kdg.catangamelogic.game_session.lobby.exception.LobbyNotFoundException;
import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.shared.UserNotFoundException;
import be.kdg.catangamelogic.shared.service.UserService;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.messaging.simp.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class LobbyController {

    private final LobbyManager lobbyManager;
    private final ModelMapper modelMapper;
    private final SimpMessagingTemplate simpMessagingTemplate;

    private final GameStateManager gameStateManager;
    private final TokenConverter tokenConverter;
    private final UserService userService;

    public LobbyController(LobbyManager lobbyManager, ModelMapper modelMapper, SimpMessagingTemplate simpMessagingTemplate, GameStateManager gameStateManager, TokenConverter tokenConverter, UserService userService) {
        this.lobbyManager = lobbyManager;
        this.modelMapper = modelMapper;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.gameStateManager = gameStateManager;
        this.tokenConverter = tokenConverter;
        this.userService = userService;
    }


    /**
     * Adds the user of the token to the lobby with the url
     *
     * @param token is the message that is sendPersonal, contains username as subject and email as Claim
     * @param url   is the url of the lobby that needs to be joined
     * @throws LobbyNotFoundException means that the lobby hasn't been created or that the lobby isn't available anymore
     */
    @MessageMapping("/lobby/{url}")
    public void joinLobby(String token, @DestinationVariable("url") String url) throws LobbyNotFoundException {
        User user = userService.findExistingOrCreateUser(token);

        Lobby lobby = lobbyManager.joinLobby(url, user);

        simpMessagingTemplate.convertAndSend("/topic/lobby/" + url,
                lobby);
    }

    /**
     * Adds a computer player to the lobby
     *
     * @param url   is the url of the lobby that needs to be joined
     * @throws LobbyNotFoundException means that the lobby hasn't been created or that the lobby isn't available anymore
     */
    @MessageMapping("/lobby/joinComputer/{url}")
    public void joinComputerToLobby(@DestinationVariable("url") String url) throws LobbyNotFoundException {
        Lobby lobby = lobbyManager.addComputerPlayer(url);

        simpMessagingTemplate.convertAndSend("/topic/lobby/" + url,
                lobby);
    }

    /**
     * Removes the user of the token from the lobby with the url
     *
     * @param token is the message that is sendPersonal, contains username as subject and email as Claim
     * @param url   is the url of the lobby that needs to be joined
     * @throws LobbyNotFoundException means that the lobby hasn't been created or that the lobby isn't available anymore
     */
    @MessageMapping("/lobby/leave/{url}")
    public void leaveLobby(String token, @DestinationVariable("url") String url) throws LobbyNotFoundException, UserNotFoundException {
        User user = userService.findExistingOrCreateUser(token);

        System.out.println(user);
        Lobby lobby = lobbyManager.leavePlayer(url, user);

        simpMessagingTemplate.convertAndSend("/topic/lobby/" + url, lobby);

        if (lobby.isClosed()) {
            lobbyManager.removeLobby(lobby);
        }
    }


    /**
     * @param body  Empty body
     * @param token the user that creates the lobby, will be the host of the lobby
     * @return the url of the created lobby
     */
    @PostMapping("/lobby/create")
    public ResponseEntity<String> createLobby(@RequestBody String body, @RequestHeader("Authorization") String token) {
        User user = userService.findExistingOrCreateUser(token);
        String url = lobbyManager.createLobby(user);
        return new ResponseEntity<String>(url, HttpStatus.OK);
    }

    @GetMapping("/lobby/available")
    public ResponseEntity<List<Lobby>> getAvailableLobbies() {
        List<Lobby> lobbies = lobbyManager.getAvailableLobbies();
        return new ResponseEntity<>(lobbies, HttpStatus.OK);
    }

    @PostMapping("/lobby/start/{url}")
    public ResponseEntity<String> startLobby(@PathVariable("url") String url, @RequestBody String body) throws CreateGameException {
        Lobby lobby = lobbyManager.getLobby(url);
        String gameurl;
        gameurl = gameStateManager.startGame(lobby);
        lobby.setGameUrl(gameurl);

        simpMessagingTemplate.convertAndSend("/topic/lobby/" + url, lobby);

        // Delete the lobby, since the game has started
        lobbyManager.removeLobby(lobby);
        return new ResponseEntity<>(gameurl, HttpStatus.OK);
    }

    @MessageMapping("/lobby/start/{url}")
    public void startLobbyMessage(String message,@PathVariable("url") String url) throws CreateGameException {
        Lobby lobby = lobbyManager.getLobby(url);
        String gameurl;
        gameurl = gameStateManager.startGame(lobby);
        lobby.setGameUrl(gameurl);

        simpMessagingTemplate.convertAndSend("/topic/lobby/" + url, lobby);

        // Delete the lobby, since the game has started
        lobbyManager.removeLobby(lobby);
    }

}
