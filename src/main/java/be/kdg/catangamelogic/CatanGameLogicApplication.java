package be.kdg.catangamelogic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatanGameLogicApplication {
    public static void main(String[] args) {
        SpringApplication.run(CatanGameLogicApplication.class, args);
    }
}

