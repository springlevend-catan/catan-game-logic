package be.kdg.catangamelogic.shared.service;

import be.kdg.catangamelogic.game_logic.action.repository.ActionRepository;
import be.kdg.catangamelogic.game_session.game.repository.GameRepository;
import be.kdg.catangamelogic.model.action.Action;
import be.kdg.catangamelogic.model.game.Game;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameService {
    private final GameRepository gameRepository;

    private final ActionRepository actionRepository;

    public GameService(GameRepository gameRepository, ActionRepository actionRepository) {
        this.gameRepository = gameRepository;
        this.actionRepository = actionRepository;
    }

    public Game saveGame(Game game) {
        return gameRepository.save(game);
    }

    public Game getGameByUrl(String url) {
        return gameRepository.findByUrl(url);
    }

    public List<Game> findByUser(String username){
        return gameRepository.findAll().stream().filter(x -> x.getPlayers().stream().anyMatch(y ->y.getUser().getUsername().equals(username))).collect(Collectors.toList());
    }

    public List<Action> getActions() {
        return actionRepository.findAll().stream().sorted(Comparator.comparing(Action::getActionTime)).collect(Collectors.toList());
    }

}
