package be.kdg.catangamelogic.shared.service;

import be.kdg.catangamelogic.game_session.lobby.repositories.PlayerRepository;
import be.kdg.catangamelogic.model.Player;
import be.kdg.catangamelogic.model.game.Game;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerService  {
    private final PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Player savePlayer(Player player){
        return playerRepository.save(player);
    }

    public List<Player> getPlayersInGame(Game game) {
        List<Player> players = playerRepository.findAllByGame(game);
        return players;
    }

    public List<Player> getPlayersWithUsername(String username){
        return playerRepository.findAllByUser_Username(username);
    }
}
