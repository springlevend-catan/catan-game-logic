package be.kdg.catangamelogic.shared.service;

import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.shared.UserNotFoundException;
import be.kdg.catangamelogic.shared.repository.UserRepository;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Value("${authentication-url}")
    private String authUrl;
    private final TokenConverter tokenConverter;


    public UserService(UserRepository userRepository, TokenConverter tokenConverter) {
        this.userRepository = userRepository;
        this.tokenConverter = tokenConverter;
    }

    public User findUserByUsername(String username){
        Optional<User> user = userRepository.findUserByUsername(username);
        return user.orElse(null);
    }

    public User findExistingOrCreateUser(String token){
        DecodedJWT decodedToken = tokenConverter.convertToken(token);
        User user = this.findUserByUsername(decodedToken.getSubject());
        if (user == null) {
            user = this.saveUser(new User(decodedToken.getSubject(), decodedToken.getClaim("email").asString()));
        }
        return user;
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }
}
