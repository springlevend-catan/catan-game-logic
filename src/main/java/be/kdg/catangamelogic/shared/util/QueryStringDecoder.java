package be.kdg.catangamelogic.shared.util;

import java.util.HashMap;
import java.util.Map;

public class QueryStringDecoder {
    public static Map<String, String> getQueryParams(String queryString) {
        Map<String, String> queryParams = new HashMap<>();

        for (String queryParam : queryString.split("&")) {
            String[] tempQueryParam = queryParam.split("=");

            queryParams.put(tempQueryParam[0], tempQueryParam[1]);
        }

        return queryParams;
    }
}
