package be.kdg.catangamelogic.shared.controller;

import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.notification.websocket.NotificationSender;
import be.kdg.catangamelogic.shared.model.UserDTO;
import be.kdg.catangamelogic.shared.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.rmi.UnexpectedException;

@RestController
@RequestMapping("/api")
public class UserController {
    private final ModelMapper modelMapper;
    private final ObjectMapper objectMapper;
    private final UserService userService;
    private final RestTemplate restTemplate = new RestTemplate();
    private final NotificationSender notificationSender;

    @Autowired
    public UserController(ModelMapper modelMapper, ObjectMapper objectMapper, UserService userService, NotificationSender notificationSender) {
        this.modelMapper = modelMapper;
        this.objectMapper = objectMapper;
        this.userService = userService;
        this.notificationSender = notificationSender;
    }

    @PostMapping("/user")
    public void createUser(@RequestBody() UserDTO userDTO) {
        System.out.println("requesta");
        System.out.println(userDTO.getEmail());
        System.out.println(userDTO.getUsername());

        User user = modelMapper.map(userDTO, User.class);

        System.out.println("user");
        System.out.println(user.getUsername());
        System.out.println(user.getEmail());

        User userCheck = userService.findUserByUsername(user.getUsername());
        if(userCheck != null){
            userService.saveUser(user);
        }
    }
}
