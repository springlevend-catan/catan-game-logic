package be.kdg.catangamelogic.shared.repository;

import java.util.*;
import be.kdg.catangamelogic.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserByUsername(String username);
}
