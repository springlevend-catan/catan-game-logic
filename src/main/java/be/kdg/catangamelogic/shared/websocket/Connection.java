package be.kdg.catangamelogic.shared.websocket;

import java.util.ArrayList;
import java.util.List;

public class Connection {
    private String webSocketId;
    private String username;
    private WebSocket webSocket;
    private List<String> sessionIds = new ArrayList<>();

    public Connection(String webSocketId, String username, WebSocket webSocket) {
        this.webSocketId = webSocketId;
        this.username = username;
        this.webSocket = webSocket;
    }

    public String getWebSocketId() {
        return webSocketId;
    }

    public void setWebSocketId(String webSocketId) {
        this.webSocketId = webSocketId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public WebSocket getWebSocket() {
        return webSocket;
    }

    public void setWebSocket(WebSocket webSocket) {
        this.webSocket = webSocket;
    }

    public void leaveSession(String sessionId) {
        sessionIds.remove(sessionId);
    }

    public boolean hasActiveSessions() {
        return sessionIds.size() > 0;
    }

    public List<String> getSessionIds() {
        return sessionIds;
    }

    public void addSessionId(String sessionId) {
        sessionIds.add(sessionId);
    }
}
