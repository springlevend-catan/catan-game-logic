package be.kdg.catangamelogic.shared.websocket;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConnectionManager {
    private List<Connection> connections = new ArrayList<>();

    public void addSession(String webSocketId, String username, String sessionId, WebSocket webSocket) {
        Connection connection = findByWebSocketIdAndUsername(webSocketId, username);

        if (connection == null) {
            connection = createInitialConnection(webSocketId, username, sessionId, webSocket);

            connections.add(connection);
        } else {
            connection.addSessionId(sessionId);
        }
    }

    private Connection createInitialConnection(String webSocketId, String username, String sessionId, WebSocket webSocket) {
        Connection connection = new Connection(webSocketId, username, webSocket);

        connection.addSessionId(sessionId);

        return connection;
    }

    public void leaveSession(String sessionId) {
        Connection connection = findBySessionId(sessionId);

        if (connection == null) {
            throw new NullPointerException("No connection found with given session ID");
        }

        connection.leaveSession(sessionId);

        if (!connection.hasActiveSessions()) {
            connections.remove(connection);
        }
    }

    public Connection findByWebSocketIdAndUsername(String token, String username) {
        for (Connection connection : connections) {
            if (connection.getWebSocketId().equals(token) && connection.getUsername().equals(username)) {
                return connection;
            }
        }

        return null;
    }

    public Connection findBySessionId(String sessionId) {
        for (Connection connection : connections) {
            for (String _sessionId : connection.getSessionIds()) {
                if (_sessionId.equals(sessionId)) {
                    return connection;
                }
            }
        }

        return null;
    }

    public Connection findByWebSocketAndUsername(WebSocket webSocket, String username) {
        for (Connection connection : connections) {
            if (connection.getUsername().equals(username) && connection.getWebSocket() == webSocket) {
                return connection;
            }
        }

        return null;
    }
}
