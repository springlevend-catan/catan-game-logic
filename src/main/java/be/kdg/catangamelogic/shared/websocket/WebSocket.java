package be.kdg.catangamelogic.shared.websocket;

public enum WebSocket {
    CHAT,
    NOTIFICATION
}
