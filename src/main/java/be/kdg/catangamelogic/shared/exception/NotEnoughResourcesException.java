package be.kdg.catangamelogic.shared.exception;

public class NotEnoughResourcesException extends Throwable {
    public NotEnoughResourcesException(String message) {
        super(message);
    }

    public NotEnoughResourcesException() {
    }
}
