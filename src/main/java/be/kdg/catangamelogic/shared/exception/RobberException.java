package be.kdg.catangamelogic.shared.exception;

public class RobberException extends Throwable{
    public RobberException(String message) {
        super(message);
    }
}
