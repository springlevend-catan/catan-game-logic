package be.kdg.catangamelogic.notification.repository;

import be.kdg.catangamelogic.notification.model.notification.personal.PersonalNotification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonalNotificationRepository<T extends PersonalNotification> extends CrudRepository<T, Long> {
    List<T> findAllByReceiver_Username(String username);
}
