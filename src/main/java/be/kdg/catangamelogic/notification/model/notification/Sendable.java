package be.kdg.catangamelogic.notification.model.notification;

public interface Sendable {
    String getJson();
}
