package be.kdg.catangamelogic.notification.model.notification.game;

import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.model.game.Game;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("chat_message")
public class ChatMessageNotification extends GameNotification {
    @ManyToOne
    private User sender;

    public ChatMessageNotification(Game game, User sender) {
        super(game);

        this.sender = sender;
    }
}
