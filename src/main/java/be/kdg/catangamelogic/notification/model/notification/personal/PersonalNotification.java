package be.kdg.catangamelogic.notification.model.notification.personal;

import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.notification.model.notification.Sendable;
import be.kdg.catangamelogic.notification.model.notification.Notification;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NoArgsConstructor
@DiscriminatorColumn(name = "personal_notification_type", discriminatorType = DiscriminatorType.STRING)
public abstract class PersonalNotification extends Notification implements Sendable {
    @ManyToOne
    private User receiver;

    @Column
    private boolean sendableWhileOffline = true;

    @Column
    private int expiresInSeconds;

    public PersonalNotification(LocalDateTime sentOn, User receiver, boolean sendableWhileOffline, int expiresInSeconds) {
        super(sentOn);

        this.receiver = receiver;
        this.sendableWhileOffline = sendableWhileOffline;
        this.expiresInSeconds = expiresInSeconds;
    }
}
