package be.kdg.catangamelogic.notification.model.notification;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class Notification {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private LocalDateTime sentOn;

    public Notification(LocalDateTime sentOn) {
        this.sentOn = sentOn;
    }

    public String getSentOnString() {
        return DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(sentOn);
    }
}
