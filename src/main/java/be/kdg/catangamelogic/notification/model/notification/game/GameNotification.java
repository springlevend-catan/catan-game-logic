package be.kdg.catangamelogic.notification.model.notification.game;

import be.kdg.catangamelogic.model.game.Game;
import be.kdg.catangamelogic.notification.model.notification.Notification;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorColumn(name = "game_notification_type", discriminatorType = DiscriminatorType.STRING)
public abstract class GameNotification extends Notification {
    @ManyToOne
    private Game game;
}
