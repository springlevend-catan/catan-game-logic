package be.kdg.catangamelogic.notification.model.notification.personal;

import be.kdg.catangamelogic.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("game_invitation")
public class GameInvitationNotification extends PersonalNotification {
    @ManyToOne
    private User sender;
    private String lobbyUrl;

    public GameInvitationNotification(LocalDateTime sentOn, User receiver, int expiresInSeconds, User sender, String lobbyUrl) {
        super(sentOn, receiver, false, expiresInSeconds);

        this.sender = sender;
        this.lobbyUrl = lobbyUrl;
    }

    @Override
    public String getJson() {
        return String.format("{\"id\": \"%d\", \"type\": \"game invitation\", \"sender\": \"%s\", \"receiver\": \"%s\", \"date\": \"%s\", \"lobby\": {\"url\": \"%s\"}, \"parameters\": [\"%s\"]}",
                getId(),
                getSender().getUsername(),
                getReceiver().getUsername(),
                getSentOnString(),
                getLobbyUrl(),
                getSender().getUsername()
        );
    }
}
