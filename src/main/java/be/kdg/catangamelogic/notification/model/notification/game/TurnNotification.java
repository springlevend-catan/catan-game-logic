package be.kdg.catangamelogic.notification.model.notification.game;

import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.model.game.Game;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("turn")
public class TurnNotification extends GameNotification {
    @ManyToOne
    private User whoseTurnUser;

    @Column
    private int turnDurationInSeconds;

    public TurnNotification(Game game, User whoseTurnUser, int turnDurationInSeconds) {
        super(game);

        this.whoseTurnUser = whoseTurnUser;
        this.turnDurationInSeconds = turnDurationInSeconds;
    }
}
