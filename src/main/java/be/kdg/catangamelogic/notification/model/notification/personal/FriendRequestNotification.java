package be.kdg.catangamelogic.notification.model.notification.personal;

import be.kdg.catangamelogic.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.io.IOException;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("friend_request")
public class FriendRequestNotification extends PersonalNotification {
    @ManyToOne
    private User sender;

    @Column
    private boolean read = false;

    public FriendRequestNotification(LocalDateTime sentOn, User receiver, int expiresInSeconds, User sender) {
        super(sentOn, receiver, true, expiresInSeconds);

        this.sender = sender;
    }

    @Override
    public String getJson() {
        return String.format("{\"id\": \"%d\", \"type\": \"friend request\", \"sender\": \"%s\", \"receiver\": \"%s\", \"date\": \"%s\", \"parameters\": [\"%s\"]}",
                getId(),
                getSender().getUsername(),
                getReceiver().getUsername(),
                getSentOnString(),
                getSender().getUsername()
        );
    }
}
