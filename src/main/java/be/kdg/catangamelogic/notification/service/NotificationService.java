package be.kdg.catangamelogic.notification.service;

import be.kdg.catangamelogic.notification.model.notification.personal.GameInvitationNotification;
import be.kdg.catangamelogic.notification.repository.PersonalNotificationRepository;
import be.kdg.catangamelogic.notification.model.notification.personal.FriendRequestNotification;
import be.kdg.catangamelogic.notification.model.notification.personal.PersonalNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class NotificationService {
    private PersonalNotificationRepository<FriendRequestNotification> friendRequestNotificationRepo;
    private PersonalNotificationRepository<PersonalNotification> personalNotificationRepo;
    private PersonalNotificationRepository<GameInvitationNotification> gameInvitationRepo;
    private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    @Autowired
    public NotificationService(PersonalNotificationRepository<FriendRequestNotification> friendRequestNotificationRepo, PersonalNotificationRepository<PersonalNotification> personalNotificationRepo, PersonalNotificationRepository<GameInvitationNotification> gameInvitationRepo) {
        this.friendRequestNotificationRepo = friendRequestNotificationRepo;
        this.personalNotificationRepo = personalNotificationRepo;
        this.gameInvitationRepo = gameInvitationRepo;
    }

    public List<FriendRequestNotification> getFriendRequestNotifications(String username) {
        return friendRequestNotificationRepo.findAllByReceiver_Username(username);
    }

    public void save(PersonalNotification personalNotification) {
        personalNotificationRepo.save(personalNotification);
    }

    public List<PersonalNotification> getAll(String username) {
        return personalNotificationRepo.findAllByReceiver_Username(username);
    }

    public FriendRequestNotification getFriendRequestNotificationById(long id) {
        return friendRequestNotificationRepo.findById(id).orElse(null);
    }

    public void deleteNotification(PersonalNotification notification) {
        personalNotificationRepo.delete(notification);
    }

    public FriendRequestNotification getPendingFriendRequest(String senderUsername, String receiverUsername) {
        for (FriendRequestNotification notification : friendRequestNotificationRepo.findAllByReceiver_Username(receiverUsername)) {
            if (notification
                    .getSender()
                    .getUsername()
                    .equals(senderUsername)) {
                return notification;
            }
        }

        return null;
    }

    public void scheduleDestruction(PersonalNotification notification) {
        int expiresInSeconds = notification.getExpiresInSeconds();

        if (expiresInSeconds == 0) {
            return;
        }

        scheduler.schedule(() -> personalNotificationRepo.delete(notification), expiresInSeconds, TimeUnit.SECONDS);
    }

    public boolean gameInvitationPending(GameInvitationNotification notification) {
        List<GameInvitationNotification> gameInvitationNotifications = gameInvitationRepo.findAllByReceiver_Username(notification
                .getReceiver()
                .getUsername()
        );

        for (GameInvitationNotification _notification : gameInvitationNotifications) {
            String _notificationUsername = _notification
                    .getSender()
                    .getUsername();

            String notificationUsername = notification
                    .getSender()
                    .getUsername();

            String _lobbyUrl = _notification.getLobbyUrl();
            String lobbyUrl = notification.getLobbyUrl();

            if (_notificationUsername.equals(notificationUsername) && _lobbyUrl.equals(lobbyUrl)) {
                return true;
            }
        }

        return false;
    }

    public GameInvitationNotification getGameInvitationById(long id) {
        return gameInvitationRepo.findById(id).orElse(null);
    }
}
