package be.kdg.catangamelogic.notification;

import be.kdg.catangamelogic.model.User;
import be.kdg.catangamelogic.notification.model.notification.personal.FriendRequestNotification;
import be.kdg.catangamelogic.notification.model.notification.personal.GameInvitationNotification;
import be.kdg.catangamelogic.notification.model.notification.personal.PersonalNotification;
import be.kdg.catangamelogic.notification.service.NotificationService;
import be.kdg.catangamelogic.notification.websocket.NotificationSender;
import be.kdg.catangamelogic.shared.model.UserDTO;
import be.kdg.catangamelogic.shared.service.UserService;
import be.kdg.catangamelogic.shared.token.TokenConverter;
import be.kdg.catangamelogic.shared.websocket.Connection;
import be.kdg.catangamelogic.shared.websocket.ConnectionManager;
import be.kdg.catangamelogic.shared.websocket.WebSocket;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.rmi.UnexpectedException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
public class NotificationController {
    private NotificationService notificationService;
    private NotificationSender notificationSender;
    private ConnectionManager connectionMannager;
    private TokenConverter tokenConverter;
    private RestTemplate restTemplate = new RestTemplate();
    private UserService userService;
    private ObjectMapper mapper;

    @Autowired
    public NotificationController(NotificationService notificationService, NotificationSender notificationSender, ConnectionManager connectionManager, TokenConverter tokenConverter, UserService userService, ObjectMapper mapper) {
        this.notificationService = notificationService;
        this.notificationSender = notificationSender;
        this.connectionMannager = connectionManager;
        this.tokenConverter = tokenConverter;
        this.userService = userService;
        this.mapper = mapper;
    }

    @CrossOrigin("http://localhost:4200")
    @GetMapping("/user/{token}/notifications")
    public ResponseEntity<String> getPersonalNotifications(@PathVariable String token) throws IOException {
        String username = tokenConverter.convertToken(token).getSubject();

        // Get notifications
        List<PersonalNotification> notifications = notificationService.getAll(username);

        notifications.add(new FriendRequestNotification(LocalDateTime.now(), userService.findExistingOrCreateUser(token), 0, new User("test", "test")));

        // Create ArrayNode to contain JSON representation of notification
        ArrayNode notificationsNode = mapper.createArrayNode();

        // Add notification JSON to notificationsNode
        for (PersonalNotification notification : notifications) {
            notificationsNode.add(mapper.readTree(notification.getJson()));
        }

        // Send ArrayNode as JSON
        return new ResponseEntity<>(notificationsNode.toString(), HttpStatus.OK);
    }

    private <T> ResponseEntity<T> sendRequest(String token, String to, String body, HttpMethod httpMethod, Class<T> expectedClass) {
        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", token);

        HttpEntity<String> entity = new HttpEntity<>(body, headers);

        return restTemplate.exchange(to, httpMethod, entity, expectedClass);
    }

    @PostMapping("/user/{token}/send-friend-request")
    public ResponseEntity<String> sendFriendRequest(@PathVariable String token, @RequestBody String receiverUsername) throws IOException {
        User sender = userService.findExistingOrCreateUser(token);

        if (sender == null) {
            throw new NullPointerException("User doesn't exist");
        }

        String senderUsername = sender.getUsername();

        // Can't add self, so send error response
        if (sender.getUsername().equals(receiverUsername)) {
            return new ResponseEntity<>("Can't add self as friend", HttpStatus.IM_USED);
        }

        // Check if a friend request is already pending
        if (notificationService.getPendingFriendRequest(senderUsername, receiverUsername) != null) {
            String error = String.format("A friend request with %s is already pending", receiverUsername);

            return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
        }

        // Check if receiver is in the database
        if (userService.findUserByUsername(receiverUsername) == null) {
            userService.saveUser(new User(receiverUsername, null));
        }

        // Check if the user already has the receiver as a friend
        String route = String.format("http://localhost:5000/user/%s/has-friend", receiverUsername);

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", token);

        HttpEntity<String> entity = new HttpEntity<>(receiverUsername, headers);

        ResponseEntity<Boolean> response = restTemplate.exchange(route, HttpMethod.POST, entity, Boolean.class);

        // Throw error if response was bad
        if (response.getBody() == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (!response.getBody()) {
            User receiver = userService.findUserByUsername(receiverUsername);

            if (userService.findUserByUsername(receiver.getUsername()) == null) {
                userService.saveUser(receiver);
            }

            FriendRequestNotification notification = new FriendRequestNotification(LocalDateTime.now(), receiver, 0, sender);

            return notificationSender.send(notification);
        } else {
            return new ResponseEntity<>("Error adding friend", HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/user/{token}/add-friend")
    public ResponseEntity<String> addFriend(@PathVariable String token, @RequestParam boolean accepted, @RequestParam long id) throws IOException {
        User user = userService.findExistingOrCreateUser(token);

        if (user == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        FriendRequestNotification notification = notificationService.getFriendRequestNotificationById(id);

        if (notification == null) {
            throw new UnexpectedException("Notification doesn't exist");
        }

        if (accepted) {
            String route = String.format("http://localhost:5000/user/%s/add-friend", token);
            String body = notification.getSender().getUsername();

            HttpHeaders headers = new HttpHeaders();

            headers.set("Authorization", token);

            HttpEntity<String> entity = new HttpEntity<>(body, headers);

            ResponseEntity<String> response = restTemplate.exchange(route, HttpMethod.POST, entity, String.class);

            if (response.getStatusCode() == HttpStatus.OK) {
                notificationService.deleteNotification(notification);

                return new ResponseEntity<>("Friend request accepted", HttpStatus.OK);
            }

            return new ResponseEntity<>("Something's gone wrong", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            notificationService.deleteNotification(notification);

            return new ResponseEntity<>("Friend request declined", HttpStatus.OK);
        }
    }

    @GetMapping("/user/{token}/connected-friends")
    public ResponseEntity<List<UserDTO>> getConnectedFriends(@PathVariable String token) throws IOException {
        User user = userService.findExistingOrCreateUser(token);

        if (user == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        String route = String.format("http://localhost:5000/user/%s/friends", token);

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", token);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<UserDTO>> response = restTemplate.exchange(route, HttpMethod.GET, entity, new ParameterizedTypeReference<List<UserDTO>>(){});

        if (response.getBody() == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (response.getStatusCode() == HttpStatus.CONFLICT) {
            return response;
        } else {
            List<UserDTO> connectedUsers = new ArrayList<>();

            for (UserDTO userDto : response.getBody()) {
                Connection connection = connectionMannager.findByWebSocketAndUsername(WebSocket.NOTIFICATION, userDto.getUsername());

                if (connection != null) {
                    connectedUsers.add(userDto);
                }
            }

            return new ResponseEntity<>(connectedUsers, HttpStatus.OK);
        }
    }

    @PostMapping("/user/{token}/invite-to-lobby")
    public ResponseEntity<String> inviteToLobby(@PathVariable String token, @RequestParam String url, @RequestBody String username) throws IOException {
        if (url == null) {
            throw new UnexpectedException("Missing lobby URL as query parameter");
        }

        User sender = userService.findExistingOrCreateUser(token);

        if (sender == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        User receiver = userService.findUserByUsername(username);

        if (receiver == null) {
            userService.saveUser(new User(username, null));

            receiver = userService.findUserByUsername(username);
        }

        GameInvitationNotification notification = new GameInvitationNotification(LocalDateTime.now(), receiver, 60, sender, url);

        if (notificationService.gameInvitationPending(notification)) {
            String message = String.format("A game invitation for lobby %s to %s is already pending.",
                    notification.getLobbyUrl(),
                    notification.getReceiver().getUsername()
            );

            return new ResponseEntity<>(message, HttpStatus.FORBIDDEN);
        }

        return notificationSender.send(notification);
    }

    @PostMapping("/user/{token}/join-game")
    public ResponseEntity<String> joinGame(@PathVariable String token, @RequestParam boolean accepted, @RequestParam long id) throws UnexpectedException {
        User user = userService.findExistingOrCreateUser(token);

        if (user == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        GameInvitationNotification notification = notificationService.getGameInvitationById(id);

        if (notification == null) {
            throw new UnexpectedException("Notification expired/doesn't exist");
        }

        notificationService.deleteNotification(notification);

        if (accepted) {
            return new ResponseEntity<>(notification.getLobbyUrl(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }
}
