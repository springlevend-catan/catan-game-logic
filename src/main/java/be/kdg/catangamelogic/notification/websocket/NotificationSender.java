package be.kdg.catangamelogic.notification.websocket;

import be.kdg.catangamelogic.notification.model.notification.Notification;
import be.kdg.catangamelogic.shared.websocket.WebSocket;
import be.kdg.catangamelogic.shared.websocket.Connection;
import be.kdg.catangamelogic.notification.model.notification.personal.FriendRequestNotification;
import be.kdg.catangamelogic.notification.model.notification.personal.PersonalNotification;
import be.kdg.catangamelogic.notification.service.NotificationService;
import be.kdg.catangamelogic.shared.websocket.ConnectionManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.io.IOException;
import java.rmi.UnexpectedException;

@Component
public class NotificationSender {
    private NotificationService notificationService;
    private SimpMessagingTemplate messagingTemplate;
    private ConnectionManager connectionManager;
    private ObjectMapper mapper;

    @Autowired
    public NotificationSender(NotificationService notificationService, SimpMessagingTemplate messagingTemplate, ConnectionManager connectionManager, ObjectMapper mapper) {
        this.notificationService = notificationService;
        this.messagingTemplate = messagingTemplate;
        this.connectionManager = connectionManager;
        this.mapper = mapper;
    }

    public ResponseEntity<String> send(PersonalNotification notification) throws IOException {
        if (notification == null) { // Throw exception if notification is null
            throw new UnexpectedException("Notification can't be null");
        }

        String receiverUsername = notification
                .getReceiver()
                .getUsername();

        // Some notifications like the friend request are sendable even if offline. A game invitation request, however,
        // would be entirely pointless if the user is offline.
        if (notification.isSendableWhileOffline()) {
            // Get the connection to the WebSocket of the receiver
            Connection connection = connectionManager.findByWebSocketAndUsername(WebSocket.NOTIFICATION, receiverUsername);

            // Save our notification to the database
            notificationService.save(notification);
            notificationService.scheduleDestruction(notification);

            // If the user is online, send to their WebSocket
            if (connection != null) {
                String route = String.format("/notifications/%s", connection.getWebSocketId());

                messagingTemplate.convertAndSend(route, notification.getJson());
            }

            return new ResponseEntity<>("Notification sent", HttpStatus.OK);
        } else {
            Connection connection = connectionManager.findByWebSocketAndUsername(WebSocket.NOTIFICATION, receiverUsername);

            if (connection != null) {
                String route = String.format("/notifications/%s", connection.getWebSocketId());

                notificationService.save(notification);
                notificationService.scheduleDestruction(notification);

                messagingTemplate.convertAndSend(route, notification.getJson());

                return new ResponseEntity<>("Notification sent", HttpStatus.OK);
            } else {
                String message = String.format("%s is offline", receiverUsername);

                return new ResponseEntity<>(message, HttpStatus.FORBIDDEN);
            }
        }
    }

    @EventListener
    public void onNotificationSubscribe(SessionSubscribeEvent event) throws UnexpectedException {
        // Get ID of WebSocket session
        String sessionId = (String) event
                .getMessage()
                .getHeaders()
                .get("simpSessionId");

        Connection connection = connectionManager.findBySessionId(sessionId);

        if (sessionId == null || connection == null || connection.getWebSocket() != WebSocket.NOTIFICATION) {
            return;
        }

        // Send friend request notifications that hadn't been sent yet due to not being connected to the WebSocket
        // (e.g. because you're offline)
        for (PersonalNotification notification : notificationService.getAll(connection.getUsername())) {
            try {
                send(notification);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @EventListener
    public void onNotificationDisconnect(SessionDisconnectEvent event) {
        String sessionId = event.getSessionId();

        Connection connection = connectionManager.findBySessionId(sessionId);

        if (connection == null) {
            throw new NullPointerException("Connection doesn't exist/is null");
        }

        if (connection.getWebSocket() != WebSocket.NOTIFICATION) {
            return;
        }

        connectionManager.leaveSession(sessionId);
    }
}
